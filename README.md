# MASTEROPPGAVE V2016 - Lars-Erik Opdal #

### What is this repository for? ###

Dette nettstedet inneholder alle programmer og skripts som er brukt i masteroppgaven,
Parallelle beregninger i delt og distribuert minne.
Programmene kan kjøre lokalt på egen maskin, eller i Abelklyngen dersom man har tilgang.
Alle programmer har en bruksanvisning og må kjøres på en Linux maskin.

### How do I get set up? ###

--------------------------
RUNNING ON LOCAL COMPUTER:
--------------------------

Download and compile opemMPI:

1.https://www.open-mpi.org/software/ompi/v1.10/downloads/openmpi-1.10.2.tar.bz2

2.gunzip -c openmpi-1.10.2.tar.gz | tar xf -

3.cd openmpi-1.10.2

4../configure --prefix=/usr/local

5.make all install

COMPILE a program: 
mpicc <file> -o <output> 

RUN a program:
mpirun -np <n proc> ./<output>

----------------------
RUNNING ON ABELCLUSTER
----------------------

COMPILE: 
mpicc <file> -o <output> 

RUN:
sbatch run.slurm

### Who do I talk to about this repo? ###

Lars-Erik Opdal