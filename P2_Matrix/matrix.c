/*Skrevet av Lars-Erik Opdal, V2015*/

#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include <omp.h>

#define BLOCK_A 999
#define BLOCK_B 888
#define BLOCK_C 777
#define DEBUG 1

/*For debugging; bare skriv "d" istedefor "printf"*/
#ifdef DEBUG
#define d printf
#else
#define d
#endif
/****************/

typedef struct{
	double** matrix;
	int m; //Maks dimensjon lendge
	int n; //Maks dimensjon bredde
	int m_a; //Faktisk dimensjon
}matrix_t; 

void initMatrix(matrix_t*, int, int);
void freeMatrix(matrix_t*);
void solved(matrix_t, matrix_t);
void printDimensions(matrix_t*, char*);
void findRows(int*, int*, int, int);
void fillEmptyRows(matrix_t*, int);
void useBuffer(matrix_t*, double*);
void partitionMatrix(int, int, int, int*, int*);
void transposeMatrix(matrix_t*, matrix_t*);
void multiply_matrices_transpose(matrix_t*, matrix_t*, matrix_t*);
void read_matrix_binaryformat (char*, double***, int*, int*);
void write_matrix_binaryformat (char*, double**, int, int);
void *dalloc (size_t, size_t, size_t);
void dalloc_free (void *);

int NUM_PROCS=-1;	//Global variabel for antall prosesser

int main(int argc, char* argv[]){
	int l, m, n;
	matrix_t A;
	matrix_t B;
	matrix_t B_transpose;
	matrix_t C;
	matrix_t C2;
	matrix_t A_block;
	matrix_t B_block;
	matrix_t C_block;
	int* A_rows;
	int* Bt_rows;
	int A_max_dim;
	int B_max_dim;

	MPI_Status status;
	int my_rank;
	int num_procs;
	double t1,t2,t3,t4; 

	if(argc<3){
		printf("write input/output name\n");
		return -1;
	}

	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
	MPI_Comm_size(MPI_COMM_WORLD, &num_procs);

	NUM_PROCS=num_procs;
	
	//Pekkere for for antall rader og elementer som skal sendes ut med scatterv
	int* values_per_proc_A = malloc(num_procs*sizeof(int));
	int* displacement_A = malloc(num_procs*sizeof(int));	
	int* values_per_proc_B = malloc(num_procs*sizeof(int));
	int* displacement_B = malloc(num_procs*sizeof(int));
	A_rows = malloc(num_procs*sizeof(int));
	Bt_rows = malloc(num_procs*sizeof(int));

	//Root leser matrise fra fil og alloker arrays
	if(my_rank==0) {
		char* in_file_A = argv[1];
		char* in_file_B = argv[2];
		read_matrix_binaryformat (in_file_A, &A.matrix, &m, &l);
		read_matrix_binaryformat (in_file_B, &B.matrix, &l, &n);
		A.m = m; A.n = l; B.m = l; B.n = n;
		initMatrix(&C, m, n);
		transposeMatrix(&B, &B_transpose);	//Velger å transopnere matrisen for kunne bruke scarrerv med matrise B
		initMatrix(&C2, m, n);

		t1=MPI_Wtime();
		multiply_matrices_transpose(&A, &B_transpose, &C2);			
		t2=MPI_Wtime();
		d("Sequential time = %5.2f ms\n",1000*(t2-t1));		

		t3=MPI_Wtime();	//starter stoppeklokke for paralell beregning
	}

	MPI_Barrier(MPI_COMM_WORLD);	//brukes for synkoriser av tidtagning

	MPI_Bcast(&m,1,MPI_INT,0,MPI_COMM_WORLD);
	MPI_Bcast(&l,1,MPI_INT,0,MPI_COMM_WORLD);
	MPI_Bcast(&n,1,MPI_INT,0,MPI_COMM_WORLD);

	//Disse funksjonsen beregner antall rader/ kolonner som skal fordeles
	findRows(A_rows, &A_max_dim, num_procs, m);
	findRows(Bt_rows, &B_max_dim, num_procs, n);

	if (my_rank == 0) {
		//Root berenger hvordan matrisen skal deles opp og fyller pekerne med denne info:
		partitionMatrix(m, l, num_procs,values_per_proc_A, displacement_A);
		partitionMatrix(n,l, num_procs, values_per_proc_B, displacement_B);
	}

	MPI_Bcast(values_per_proc_A, num_procs, MPI_INT, 0, MPI_COMM_WORLD);
	MPI_Bcast(displacement_A, num_procs, MPI_INT, 0, MPI_COMM_WORLD);
	MPI_Bcast(values_per_proc_B, num_procs, MPI_INT, 0, MPI_COMM_WORLD);
	MPI_Bcast(displacement_B, num_procs, MPI_INT, 0, MPI_COMM_WORLD);

	initMatrix(&A_block, A_max_dim, l);
	initMatrix(&B_block, B_max_dim, l);
	initMatrix(&C_block, A_max_dim, B_max_dim);

	if (my_rank != 0) {	
		initMatrix(&A, m, l);
		transposeMatrix(&B, &B_transpose); 	//Velger å transopnere matrisen for kunne bruke scarrerv med matrise B
	}
	//Bruker scatterv for å fordele A og B matrisen mellom alle prosesser, 
	//den sender forsjellig antall elementer basert på antall prosesser
	MPI_Scatterv(A.matrix[0], values_per_proc_A, displacement_A, MPI_DOUBLE, 
			A_block.matrix[0], values_per_proc_A[my_rank], MPI_DOUBLE, 0, MPI_COMM_WORLD);
	MPI_Scatterv(B_transpose.matrix[0], values_per_proc_B, displacement_B, MPI_DOUBLE, 
			B_block.matrix[0], values_per_proc_B[my_rank], MPI_DOUBLE, 0, MPI_COMM_WORLD); 

	//Fyller A og B blokk med padding for å forenkle mapping av C i root
	fillEmptyRows(&A_block, A_rows[my_rank]);
	fillEmptyRows(&B_block, Bt_rows[my_rank]);

	//De andre prosessene trenger ikke lenger A og B
	if (my_rank != 0) {	
		freeMatrix(&A);
		freeMatrix(&B_transpose);
	}

	//Beregner radene/ blokene til C ved at alle beregner sin egen C blokk og sender den til root. Dette gjørers antall prosesser ganger.
	//Etter det blir B blokka sent til nabo prosess som trenger den for å beregne neste C blokk.
	//B blokkene til prosess[i] og prosess[i+1] må forsjellig størrelse for at de alle skal passe inn i C tilslutt.

	//Velger å fylle opp C diagonalt fremfor radvis, d_index definer hvem diagonal/ iterasjon vi er iforhold til antall prosesser
	int d_index; 
	for(d_index=0; d_index < num_procs; d_index++){
		int i,j; 
		//Dimensjonene for nåværende C blokk for blir beregnet:
		int C_block_m; 
		int C_block_n; 

		multiply_matrices_transpose(&A_block, &B_block, &C_block);

		//Root mapper sin egen del av C til seg selv
		if(my_rank==0){ 			
			C_block_m = A_rows[0];
			C_block_n = Bt_rows[d_index];

			int j_new = 0;
			int k;
			for (k = 0; k < d_index; k++)
				j_new += Bt_rows[k];


			for(i = 0; i < C_block_m; i++)
				for(j= j_new; j< j_new + C_block_n;j++)
					C.matrix[i][j]=C_block.matrix[i][j - j_new];
		}

		//Her mappes resten av C blokkene til root sin C
		if(my_rank==0){
			int p_index; 	//Brukes som src/ dst for å sende C blokk til root fordi root ikke kan sende til seg selv.
			int u = i;
			int v = j;
			for(p_index = 1; p_index < num_procs; p_index++) {
				int new_u, new_v;
				C_block_m = A_rows[p_index];
				C_block_n = Bt_rows[(d_index + p_index) % num_procs];

				//Root tar imot beregnet C blokk fra alle andre prosesser
				MPI_Recv(C_block.matrix[0],C_block.m*C_block.n, MPI_DOUBLE, p_index, BLOCK_C * d_index, MPI_COMM_WORLD, &status);

				new_u = u;
				if (v == C.n) 
					new_v = 0;
				else
					new_v = v;
				for(u = new_u; u< new_u + C_block_m; u++){ 
					for(v = new_v; v < new_v + C_block_n; v++){
						C.matrix[u][v]=C_block.matrix[u-new_u][v-new_v];
					}	
				}
			}
		} else{
			MPI_Send(C_block.matrix[0], C_block.m*C_block.n, MPI_DOUBLE, 0, BLOCK_C * d_index, MPI_COMM_WORLD);	
		}
		//Her kommuniseres B blokken mellom alle prossesne inkludert root
		//Bruker et buffer for å ikke skape deadlock pga. buffer blir fullt dersom man beregner store matriser.
		//Dette forkommer pga. MPI har et berenset kapasistet for sitt buffer i blokerende send/recv funksjoner.
		if (my_rank % 2 == 0) {
			if (my_rank == 0) {
				MPI_Send(B_block.matrix[0], B_block.m*B_block.n, MPI_DOUBLE, num_procs - 1, d_index + 1, MPI_COMM_WORLD);
				MPI_Recv(B_block.matrix[0], B_block.m*B_block.n, MPI_DOUBLE, 1, d_index + 1, MPI_COMM_WORLD, &status);		

			} else if (my_rank == num_procs-1){
				double* buffer = malloc(B_block.m*B_block.n*sizeof(double));
				MPI_Recv(B_block.matrix[0], B_block.m*B_block.n, MPI_DOUBLE, 0, d_index + 1, MPI_COMM_WORLD, &status);		
				MPI_Send(B_block.matrix[0], B_block.m*B_block.n, MPI_DOUBLE, my_rank - 1, d_index + 1, MPI_COMM_WORLD);
				useBuffer(&B_block, buffer);
			} else {
				MPI_Send(B_block.matrix[0], B_block.m*B_block.n, MPI_DOUBLE, my_rank - 1, d_index + 1, MPI_COMM_WORLD);
				MPI_Recv(B_block.matrix[0], B_block.m*B_block.n, MPI_DOUBLE, my_rank + 1, d_index + 1, MPI_COMM_WORLD, &status);		
			}								
		} else if (my_rank % 2 == 1) { 
			double* buffer = malloc(B_block.m*B_block.n*sizeof(double));
			MPI_Recv(buffer, B_block.m*B_block.n, MPI_DOUBLE, (my_rank + 1) % num_procs, d_index + 1, MPI_COMM_WORLD, &status);		
			MPI_Send(B_block.matrix[0], B_block.m*B_block.n, MPI_DOUBLE, my_rank - 1, d_index + 1, MPI_COMM_WORLD); // riktig
			useBuffer(&B_block, buffer);
		}
	} 

	if (my_rank==0){
		t4=MPI_Wtime();
		solved(C,C2);
		d("Parallel time = %5.2f ms\n",1000*(t4-t3));
		d("Speed up = %5.2f \n",(t2-t1)/(t4-t3));
		char* output="output.bin";
		write_matrix_binaryformat (output, C.matrix, m, n);
	}

	if (my_rank == 0) {
		freeMatrix(&A);
		freeMatrix(&B);
		freeMatrix(&B_transpose);
		freeMatrix(&C);
		freeMatrix(&C2);
	}

	freeMatrix(&A_block);
	freeMatrix(&B_block);
	freeMatrix(&C_block);

	MPI_Finalize();
	return 0;
}

void initMatrix(matrix_t* A, int m, int n){
	A->matrix=dalloc(m,n,sizeof(double));
	A->m=m;
	A->n=n;
}

void freeMatrix(matrix_t* A){
	A->m=0;
	A->n=0;
	dalloc_free(A->matrix);
}

void solved(matrix_t C1, matrix_t C2){
	int i,j;
	for(i=0;i<C1.m;i++)
		for(j=0;j<C1.n;j++)
			if(C1.matrix[i][j]!=C2.matrix[i][j]){
				d("Failed!\n");
				return;
			}
	d("Matrix solved correct!\n");
}

void printDimensions(matrix_t* A, char* matrix_name) {
	printf("Matrix %s has dimensions m=%d and n= %d \n", matrix_name, (int)A->m, (int)A->n);
}

void findRows(int* dimensions, int *max_dim, int num_procs, int m) {
	int i;
	*max_dim = -1;
	for (i = 0; i < num_procs; i++) {
		dimensions[i] = m / num_procs;
		if (i < m % num_procs) {
			dimensions[i]++;
		}
		if (*max_dim < dimensions[i])
			*max_dim = dimensions[i];
	}
}

void partitionMatrix(int m, int n, int num_procs, int* values_per_proc, int* displacement){
	int i; int j;
	for (i =0; i < num_procs; i++) { 
		values_per_proc[i] = n * (m/num_procs);
		if(i < (m % num_procs))
			values_per_proc[i] = values_per_proc[i] + n; 
	}

	for (i = 0; i < num_procs; i++) { 
		displacement[i] = 0; 
	}
	for (i = 0; i < num_procs; i++) {
		for (j = 0; j < i; j++) {
			displacement[i] += values_per_proc[j];
		}
	}
}

void transposeMatrix(matrix_t* A, matrix_t* A_transpose){
	int i,j;
	initMatrix(A_transpose,A->n,A->m);
	for (i = 0; i < A->m; i++) 
		for (j = 0; j < A->n; j++) 
			A_transpose->matrix[j][i]=A->matrix[i][j];
}

void fillEmptyRows(matrix_t* A, int row_dim) {
	if (A->m > row_dim) {
		int i, j;
		for (i = row_dim; i < A->m; i++) {
			for (j = 0; j < A->n; j++) {
				A->matrix[i][j] = 0;
			}
		}
	}
}

void useBuffer(matrix_t* B_block, double* buffer) {
	int i, j, k;
	k=0;
	for(i = 0; i < B_block->m; i++) {
		for(j = 0; j < B_block->n; j++) {
			B_block->matrix[i][j] = buffer[k++];
		}		
	}
}

void multiply_matrices_transpose(matrix_t* A, matrix_t* B, matrix_t* C) {
	int i, j, k;
	int m = A->m;
	int n = B->m;
	int l = A->n; 
	int nthreads = NUM_PROCS;

//Kan openMP selv finne beste chunk_size/ fordeling av oppgavene med "auto"
//Men "guided" ser ut til å være mest effektiv
#pragma omp parallel private(i, j, k) num_threads(nthreads)
#pragma omp for schedule(guided)
	for (i = 0; i < m; i++) {
		for (j = 0; j < n; j++) {
			C->matrix[i][j]=0.0;	
			for (k = 0; k < l; k++) 
				C->matrix[i][j] = C->matrix[i][j] + A->matrix[i][k] * B->matrix[j][k];
		}
	}
}

void read_matrix_binaryformat (char* filename, double*** matrix, int* num_rows, int* num_cols) {
	int i;
	FILE* fp = fopen (filename,"rb");
	fread (num_rows, sizeof(int), 1, fp);
	fread (num_cols, sizeof(int), 1, fp);

	/* storage allocation of the matrix */
	*matrix = (double**)malloc((*num_rows)*sizeof(double*));
	(*matrix)[0] = (double*)malloc((*num_rows)*(*num_cols)*sizeof(double));

	for (i=1; i<(*num_rows); i++)
		(*matrix)[i] = (*matrix)[i-1]+(*num_cols);

	/* read in the entire matrix */
	fread ((*matrix)[0], sizeof(double), (*num_rows)*(*num_cols), fp);
	fclose (fp);
}

void write_matrix_binaryformat (char* filename, double** matrix,int num_rows, int num_cols){
	FILE *fp = fopen (filename,"wb");
	fwrite (&num_rows, sizeof(int), 1, fp);
	fwrite (&num_cols, sizeof(int), 1, fp);
	fwrite (matrix[0], sizeof(double), num_rows*num_cols, fp);
	fclose (fp);
}

void *dalloc (size_t m_memb, size_t n_memb, size_t size) {
	size_t i;
	void ** retval;

	retval = malloc(m_memb * sizeof(void *));

	if (retval == NULL)
		return NULL;

	retval[0] = malloc(m_memb * n_memb * size);

	if (retval[0] == NULL) {
		free(retval);
		return NULL;
	}

	for (i = 1; i < m_memb; i++)
		retval[i] =
			((char *) retval[0]) + i * n_memb * size;

	return retval;
}

void dalloc_free (void * ptr) {
	if (ptr == NULL)
		return;

	free(* (void **) ptr);
	free(ptr);
}
