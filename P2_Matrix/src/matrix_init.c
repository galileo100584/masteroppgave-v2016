#include <stdlib.h>
void *dalloc (size_t m_memb, size_t n_memb, size_t size)
{
  size_t i;
  void ** retval;

  retval = malloc(m_memb * sizeof(void *));

  if (retval == NULL)
    return NULL;

  retval[0] = malloc(m_memb * n_memb * size);

  if (retval[0] == NULL) {
    free(retval);
    return NULL;
  }

  for (i = 1; i < m_memb; i++)
    retval[i] =
      ((char *) retval[0]) + i * n_memb * size;

  return retval;
}
void alloc(double*** matrix, int *num_rows, int *num_cols){
        int i;
        *matrix = (double**)malloc((*num_rows)*sizeof(double*));
        (*matrix)[0] = (double*)malloc((*num_rows)*(*num_cols)*sizeof(double));
        for (i=1; i<(*num_rows); i++)
                (*matrix)[i] = (*matrix)[i-1]+(*num_cols);
}
void allocBlock(double** Q, size_t m_memb, size_t n_memb, size_t size){
	Q=dalloc(m_memb,n_memb,size);
}
void alloc1D(double** vector, int num_rows){
        int i;
		double *v;
        v = malloc((num_rows)*sizeof(double*));
		*vector=v;
}
void fill2D(double** A, double** B, int num_rows, int num_cols){
        int i,j;
        int k=0;
        for(i=0;i<num_rows;i++)
                for(j=0;j<num_cols;j++){
                        A[i][j]=(double)k;
                        B[i][j]=(double)k;
                        k++;
                }
}
void fill1D(double* x, int num_rows){
        int i,j;
        int k=0;
        for(i=0;i<num_rows;i++){
        	x[i]=(double)k;
                k++;
        }
}
