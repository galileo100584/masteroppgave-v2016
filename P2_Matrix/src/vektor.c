#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <mpi.h>
#include <assert.h>


#define DEBUG 1

#ifdef DEBUG
#define d printf
#else
#define d
#endif


#include "../include/matrix_init.h"
#include "../include/matrix_math.h"
#include "../include/matrix_tools.h"

#define BLOCKS 999
#define VECTOR 888
#define RESULT 777

int main(int argc, char* argv[]){
	double **A;
	double **B;
	double **C;
	double **Q;
	double *x;
	double *y;
	int a_num_rows;
	int b_num_rows;
	int a_num_cols;
	int b_num_cols;
	int my_rank;
	int num_procs;
	double t1,t2,diff;

	//assert(argc > 0);
	if(argc < 3){
		printf("type in argv[1] and argc[2] for n x n\n");
		return -1;
	}	

	MPI_Init(&argc,&argv);
	MPI_Comm_rank(MPI_COMM_WORLD,&my_rank);
	MPI_Comm_size(MPI_COMM_WORLD,&num_procs);
	MPI_Status status;
	if(my_rank==0){
		/*char *input1=argv[1];
		  char *input2=argv[2];
		  read_matrix_binaryformat (input1, &A, &a_num_rows, &a_num_cols);	//100x50
		  read_matrix_binaryformat (input2, &B, &b_num_rows, &b_num_cols);	//50x100*/
		a_num_rows=atoi(argv[1]);
		a_num_cols=a_num_rows;	
		b_num_rows=atoi(argv[2]);
		b_num_cols=b_num_rows;

	}
	MPI_Bcast(&a_num_rows,1,MPI_INT,0,MPI_COMM_WORLD);
	MPI_Bcast(&a_num_cols,1,MPI_INT,0,MPI_COMM_WORLD);
	MPI_Bcast(&b_num_rows,1,MPI_INT,0,MPI_COMM_WORLD);
	MPI_Bcast(&b_num_cols,1,MPI_INT,0,MPI_COMM_WORLD);

	alloc(&A,&a_num_rows,&a_num_cols);
	alloc(&B,&b_num_rows,&b_num_cols);
	int block_row=2;
	int block_col=2;
	alloc(&Q,&block_row,&block_col);
	alloc(&C,&a_num_rows,&a_num_cols);
	fill2D(A,B,a_num_rows,a_num_cols);
	alloc(&C,&a_num_rows,&b_num_cols);
	alloc1D(&x,a_num_cols);
	alloc1D(&y,a_num_cols);

	//disse kan ikke brukes til funksjoner dersom de er 2D !
	//double Q[2][2] = { {0.0,0.0}, {0.0,0.0} };

	double R[2] ={0.0, 0.0};
	double P[2] = {0.0, 0.0};	//vektor for mellom regning


	if(my_rank==0){
		fill1D(x,a_num_cols);
		int i;
		int k;
		int j=2;	// j = 2 fordi hopper over første blokk ={0,1,4,5}
		int n = a_num_rows;

		//deler ut første del av matrise A 
		for(i=0;i<2;i++)
			Q[0][i]=A[0][i];
		for(i=0;i<2;i++)
			Q[1][i]=A[1][i];

		//deler ut første del av x:
		for(i=0;i<2;i++)
			R[i]=x[i];	
		//start = clock();
		t1=MPI_Wtime();
		for(k=1;k<num_procs;k++){
			//sender sender de 2 første radene til første blokk...
			if(k%2==1)
				MPI_Send(x+2, 2, MPI_DOUBLE, k, VECTOR, MPI_COMM_WORLD);
			else
				MPI_Send(x, 2, MPI_DOUBLE, k, VECTOR, MPI_COMM_WORLD);

			MPI_Send(A[0]+j, 2, MPI_DOUBLE, k, BLOCKS, MPI_COMM_WORLD);	
			//sender sender de 2 første radene til andre blokk...
			MPI_Send(A[0]+j+n, 2, MPI_DOUBLE, k, BLOCKS, MPI_COMM_WORLD);
			j+=2;
			if(j%num_procs==0)	//ferdig med en blokk rad dersom ingen rest
				j+=n;	//hopper over den rad som aledrede er tatt
		}
	}
	else{
		MPI_Recv(R, 2, MPI_DOUBLE, 0, VECTOR, MPI_COMM_WORLD,&status);
		MPI_Recv(Q[0], 2, MPI_DOUBLE, 0, BLOCKS, MPI_COMM_WORLD, &status);
		MPI_Recv(Q[1], 2, MPI_DOUBLE, 0, BLOCKS, MPI_COMM_WORLD, &status);
	}
	int i,j;
	//resultat etter steg 1:
	multiplyVector(Q, R, P, 2, 2);

	if(my_rank!=0){
		//alle sender sin mellomregning til root
		MPI_Send(P,2,MPI_DOUBLE,0,RESULT,MPI_COMM_WORLD);
	}
	else{//root tar imot
		int i,j,k;
		k=0;
		//mellomlagring for vektor
		double tmp[2];
		//legger inn første del av y fra root sin egen P
		for(i=0;i<2;i++)
			y[i]=P[i];

		//legger inn rest av vektor fra de andre procs
		for(i=1;i<num_procs;i++){
			MPI_Recv(tmp,2,MPI_DOUBLE,i,RESULT,MPI_COMM_WORLD, &status);
			//oppdatter y med verdier fra de andre procs
			if(i==1){
				for(j=0;j<2;j++){
					y[k]+=tmp[j];
					k++;
				}
			}
			else{

				for(j=0;j<2;j++){
					y[k]+=tmp[j];
					k++;
				}
				k=2;
			}
		}
	}
	if(my_rank==0){
		//diff = clock() - start;
		t2 = MPI_Wtime();
		for(i=0;i<num_procs;i++)
			d("y[%d]=%5.2f\n",i,y[i]);
		diff = t2 - t1;
//		int msec = diff * 1000 / CLOCKS_PER_SEC;
//		printf("Time taken %d seconds %d milliseconds\n", msec/1000, msec%1000);
		printf("Time elapsed: %f ms \n",(1000.0*diff) );
	}
	MPI_Finalize();
	return 0;
}
