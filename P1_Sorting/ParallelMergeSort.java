import java.util.*;
import mpi.*;

class ParallelMergeSort {
	static int INIT = 1; // Message giving size and height
	static int DATA = 2; // Message giving vector to sort
	static int ANSW = 3; // Message returning sorted vector
	static int FINI = 4; // Send permission to terminate
	static int N = 1000000;
	public static void main(String args[]) throws MPIException {
		int myRank, nProc;
		int size=0; // Size of the vector being sorted
		int[] vector = null; // Vector for parallel sort
		int[] solo = null; // Copy for sequential sort
		int[] soloRet = null; // Copy for sequential sort
		double start_par=0.0,start_sek=0.0, // Begin time
		       finish_par=0.0, // Finish parallel sort
		       finish_sek=0.0; // Finish sequential sort
				
		int argc = args.length;

		MPI.Init(args);
		//if (argc > 0)
		size = N; //Integer.parseInt(args[0]);
		myRank = MPI.COMM_WORLD.getRank();
		nProc = MPI.COMM_WORLD.getSize();

		if (myRank == 0) // Host process
		{
			int rootHt = 0, nodeCount = 1;

			while (nodeCount < nProc) {
				nodeCount += nodeCount;
				rootHt++;
			}

			System.out.println(nProc + " processes mandates root height of "+ rootHt);
			System.out.println("size: " + N);
			vector = new int[N];
			Random rand = new Random();
			for (int i = 0; i < N; i++){
				vector[i] = rand.nextInt(size);
			}
			
			//vector=getData(vector,size);
			// Capture time to sequentially sort the idential array
			solo = new int[size];
			System.arraycopy(vector, 0, solo, 0, size);
				
			//printVector("Vector",vector);
			//printVector("Solo",solo);

			start_par = MPI.wtime();
			parallelMerge(vector, size, rootHt);
			if(isSorted(vector))
				finish_par = MPI.wtime();
			else
				System.out.println("not sorted!");
			

		} else // Node process
		{
			int[] iVect = new int[2]; // Message sent as an array
			int height; // Pulled from iVect
			int parent; // Computed from myRank and height

			MPI.COMM_WORLD.recv(iVect, 2, MPI.INT, MPI.ANY_SOURCE, INIT);

			size = iVect[0]; // Isolate size
			height = iVect[1]; // and height
			vector = new int[size];

			MPI.COMM_WORLD.recv(vector, size, MPI.INT, MPI.ANY_SOURCE, DATA);

			parallelMerge(vector, size, height);
		}
		// Only the rank-0 process executes here.
		if(myRank==0){
			start_sek = MPI.wtime();
			Arrays.sort(solo);
			finish_sek = MPI.wtime();
	
			if (isSorted(vector) && isSorted(solo) )
			  System.out.println("Sorting succeeds.");
			else
			  System.out.println("SORTING FAILS.");

			double par = 0.0;
			double sek = 0.0;
			
			par = (finish_par - start_par);
			sek = (finish_sek - start_sek);
			
			System.out.println("  Parallel: " + par);
			System.out.println("Sequential: " + sek);
			System.out.println("  Speed-up:  " + sek/par);
		}
		
			MPI.Finalize();	
	}

	static void printVector(String name, int[] vector){
		System.out.println(name+":");
		for(int i = 0;i<vector.length;i++)
			System.out.println(vector[i]+" ");
	}

	static int[] insertSort(int[] array){
		for(int k =0;k<array.length-1;k++){
			int t = array[k+1];
			int i = k;
			while( i>= 0 && array[i] > t){
				array[i+1]=array[i];
				i--;
			}
			array[i+1]=t;
		}
		return array;
	}

	static int[] getData(int[] vPtr, int size) {
		vPtr = new int[size];
		Random rand = new Random();
		for (int i = 0; i < size; i++)
			vPtr[i] = rand.nextInt(size);
		return vPtr;

	}

	static boolean isSorted(int[] vector){
		for(int i=1;i<vector.length;i++)
			if(vector[i-1]>vector[i])
				return false;
		return true;
	}
	/**
	 * Parallel merge logic under MPI
	 * 
	 * The working core: each internal node ships its right-hand side to the
	 * proper node below it in the processing tree. It recurses on this function
	 * to process the left-hand side, as the node one closer to the leaf level.
	 */
	static void parallelMerge(int[] vector, int size, int myHeight) throws MPIException{
		int parent;
		int myRank, nProc;
		int nxt; 
		int rtChild=0;
		myRank = MPI.COMM_WORLD.getRank();
		nProc = MPI.COMM_WORLD.getSize();
		parent = myRank & ~(1 << myHeight);
		nxt = myHeight - 1;
		if (nxt >= 0)
			rtChild = myRank | (1 << nxt);

		if (myHeight > 0) {// Possibly a half-full node in the processing tree
			if (rtChild >= nProc){ // No right child. Move down one level
				parallelMerge(vector, size, nxt);
			}
			else {
				int left_size = size / 2, right_size = size - left_size;
				int[] leftArray = new int[left_size];
				int[] rightArray = new int[right_size];	
				int[] iVect = new int[2];
				int i, j, k; // Used in the merge logic

				System.arraycopy(vector, 0, leftArray, 0, left_size);
				System.arraycopy(vector, left_size, rightArray, 0, right_size);
				iVect[0] = right_size;
				iVect[1] = nxt;
				MPI.COMM_WORLD.send(iVect, 2, MPI.INT, rtChild, INIT);
				MPI.COMM_WORLD.send(rightArray, right_size, MPI.INT, rtChild,
						DATA);
				
				//får ikke dette til...
				/*if(left_size<51)
					leftArray=insertSort(leftArray);
				else*/
					parallelMerge(leftArray, left_size, nxt);

				MPI.COMM_WORLD.recv(rightArray, right_size, MPI.INT, rtChild,
						ANSW);

				// Merge the two results back into vector
				i = j = k = 0;
				while (i < left_size && j < right_size)
					if (leftArray[i] > rightArray[j])
						vector[k++] = rightArray[j++];
					else
						vector[k++] = leftArray[i++];
				while (i < left_size)
					vector[k++] = leftArray[i++];
				while (j < right_size)
					vector[k++] = rightArray[j++];
			}
		} else {
			Arrays.sort(vector);	//i bunnen av treet, skjer sortering sekvennsielt pa ver node
		}

		/**
		 * Note: If the computed parent is different from myRank, then this is a
		 * right-hand side and needs to be sent as a message back to its parent.
		 * Otherwise, the "communication" is done automatically because the
		 * result is generated in place.
		 */
		if (parent != myRank)
			MPI.COMM_WORLD.send(vector, size, MPI.INT, parent, ANSW);
	}

}// ending class
