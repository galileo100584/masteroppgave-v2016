 /*Skrevet av Lars-Erik Opdal V2016*/
#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>

void swap(int *i, int *j) {
	int t = *i;
	*i = *j;
	*j = t;
}
#define N 1000000
int main(int argc, char *argv[]){
	int i,a=1,b=2,tmp=0;
	double t0,t1;
	//TEST #1 swap 2 integers
int my_rank,num_procs;
	//TEST #2, send/recv 2 integers
	MPI_Init (&argc, &argv);	/* starts MPI */
	MPI_Comm_rank (MPI_COMM_WORLD, &my_rank);	/* get current process id */
	MPI_Comm_size (MPI_COMM_WORLD, &num_procs);	/* get number of processes */
	MPI_Status status;	
	a=my_rank;

	t0=MPI_Wtime();
	for(i=0;i<N;i++){
		swap(&a,&b);
	}
	t1=MPI_Wtime();
	
	if(my_rank==0)	printf("Swap 2 integers %d times = %5.2f ms\n",N,(t1-t0)*1000.0);

	t0=MPI_Wtime();
	for(i=0;i<N;i++){
		if(my_rank==0){
			MPI_Send(&a,1,MPI_INT,1,999,MPI_COMM_WORLD);
			MPI_Recv(&a,1,MPI_INT,1,999,MPI_COMM_WORLD,&status);
		}
		else{
			MPI_Recv(&a,1,MPI_INT,0,999,MPI_COMM_WORLD,&status);
			MPI_Send(&a,1,MPI_INT,0,999,MPI_COMM_WORLD);
		}
	}
	t1=MPI_Wtime();
	if(my_rank==0)	printf("send/recv 2 integers %d times = %5.2f ms\n",N,(t1-t0)*1000.0);
	MPI_Finalize();
	return 0;
}
