import java.util.*;
import mpi.*;
//import p2pmpi.mpi.*;

class Ping_pong{
	public static void main(String args[]) throws MPIException {
		int argc = args.length;
		int N = 1000000;	//10^6
		int size;
		int my_rank,num_procs;

		double t0=0.0,t1=0.0;
		MPI.Init(args);
		if (argc > 0)
			size = N; //Integer.parseInt(args[0]);
		my_rank = MPI.COMM_WORLD.getRank();
		num_procs = MPI.COMM_WORLD.getSize();
		int[] a = new int[1];
		//MPI_Status status;	ser ikke ut som java bruker status
		//TEST #2, send/recv 2 integers

		if(my_rank==0)	a[0]=999;

		System.out.println("Before send/recv:" );
		System.out.println("proc "+my_rank+" has msg = " + a[0] );

		t0=MPI.wtime();
		for(int i=0;i<N;i++){
			if(my_rank==0){
				//System.out.println("proc "+my_rank+" sending msg = " + a[0] );	
				MPI.COMM_WORLD.send(a, 1, MPI.INT, 1, 999);
				//MPI.COMM_WORLD.recv(a, 1, MPI.INT, 1, 999);
				//System.out.println("proc "+my_rank+" recv msg = " + a[0] );
				//MPI_Send(&a,1,MPI_INT,1,999,MPI_COMM_WORLD);
				//MPI_Recv(&a,1,MPI_INT,1,999,MPI_COMM_WORLD,&status);
			}
			else{
				MPI.COMM_WORLD.recv(a, 1, MPI.INT, 0, 999);
				//a[0] = 888;
				//MPI.COMM_WORLD.send(a, 1, MPI.INT, 0, 999);
				//MPI_Recv(&a,1,MPI_INT,0,999,MPI_COMM_WORLD,&status);
				//MPI_Send(&a,1,MPI_INT,0,999,MPI_COMM_WORLD);
			}
		}
		t1=MPI.wtime();
		if(my_rank==0)	System.out.println("send 1 integers "+N+" times, from p0 to p1 to p1 (ping_pong) ="+(t1-t0)*1000.0+ " ms");
		System.out.println("After send/recv:" );
		System.out.println("proc "+my_rank+" has msg = " + a[0] );


		MPI.Finalize();
	}
}
