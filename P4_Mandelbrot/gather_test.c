/* C Example */
#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
/*Example:
if image is l x m 100 x 100 = 10000 pixels
2 processes, take 10000/2 = 5000 pixels each
my_start = my_rank*(n/num_procs) 
my_end = (my_rank+1)*(n/num_procs)
int pix_count 
for x=0:l
	for y=0:m
		if(my_start <= pix_count && pix_count < my_end)
			compute pixel = image_part
			pixel_count += 1

mpi_gather(image_part,total_pixels/num_procs,hole_image,total_pixels/num_procs)
*/

int main (int argc,char *argv[])
{
	int my_rank, num_procs;
	int n=100;
	MPI_Init (&argc, &argv);	/* starts MPI */
	MPI_Comm_rank (MPI_COMM_WORLD, &my_rank);	/* get current process id */
	MPI_Comm_size (MPI_COMM_WORLD, &num_procs);	/* get number of processes */
	printf( "Hello world from process %d of %d\n", my_rank, num_procs );
	int i;
	char * array_part = (char*)malloc(n);
	
	char * array;
	if(my_rank==0) 
		array = (char*)malloc(n*num_procs);

	for(i=0;i<n;i++)
		array_part[i]=i;

	MPI_Gather(array_part, n/num_procs, MPI_CHAR,array, n/num_procs, MPI_CHAR,0, MPI_COMM_WORLD);
	if(my_rank==0)	for(i=0;i<n;i++)	printf("array[%d]=%d\n",i,array[i]);
		
	MPI_Finalize();
	return 0;
}
