#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <assert.h>

#include "../includes/util.h"
#include "../includes/randomPoints.h"

void initPoints(int n, Points *p) {
	p->n = n;
	p->xShift = 7;
	p->scaleFactor = 3;
	p->seed = 123;
	p->maxXY = max(10,(int) sqrt(n) * p->scaleFactor); // største X og Y verdi
	while ((1<<p->xShift) < p->maxXY) p->xShift++;
	p->xShift = p->xShift - 3;                    // 8 bits per byte
	p->bitArr = (unsigned char*)calloc(( (p->maxXY<<p->xShift |(p->maxXY>>3))  + 5),sizeof(int) );
}

static inline void setUsed(int x, int y, Points *p) {
	p->bitArr[(x<<p->xShift) | (y >>3)] |=  (1<<(y&7));
}

static inline int used (int x, int y, Points *p) {
	return  (p->bitArr[(x<<p->xShift) | (y >>3)] & (1<<(y&7))) != 0;
}

static int Random(int min, int max, Points *p) {
	p->seed ^= p->seed << 21;
	p->seed ^= p->seed >> 35;
	p->seed ^= p->seed << 4;

	return llabs(p->seed) % (max-min+1)+min;
}

void fyllArrayer(int *x, int *y, Points *p) {
	int next = 0;
	int xval;
	int yval;
	int maxHalve = p->maxXY/2-1;
	while (next < p->n) {
		do{
			xval = Random(0,maxHalve,p)+1;
			xval <<=1;          // now an even number
			yval = Random(0,maxHalve,p)+1;
			yval <<=1;  // now an even number
		} while (used (xval, yval,p));
		x[next] = xval;
		y[next] = yval;
		setUsed (xval,yval,p);
		next++;
	} // next point
}
