#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <assert.h>
#include <values.h>
#include <tgmath.h>

#include "../includes/util.h"
#include "../includes/context.h"
#include "../includes/nlist.h"
#include "../includes/cohull.h"
#include "../includes/functions.h"

int  findRestOfNeighbours (int a, int b, int stop, Context *context) {
	assert(context != NULL);	
	int numNB =0;
	int c = b;
	int cohullCase = (stop!=b);
	int i;

	// find all neighbours counter-clockwise around point a
	int count=0;
	do{
		count++;
		b = c;
		context->allNeighbours[numNB++] = c;
		c = findNextNeighbour (a,b, context);	//se på hva som er a og b
	} while (c != stop);
	if (cohullCase ) { // cohull case
		numNB++;
	}
	// set in neighbourlist
	int *nbToA  = (int*)calloc(numNB,sizeof(int));

	context->delaunayEdges[a]=(int*)malloc(sizeof(int)*numNB);

	if (cohullCase ) { // cohull case
		context->allNeighbours[numNB-1] = stop;
	}

	for (i = 0; i < numNB; i++){
		nbToA[i] = context->allNeighbours[i];
	}

	context->delaunayEdges_i_length=context->n;		
	context->delaunayEdges_j_length=numNB;

	for(i=0;i<numNB;i++)
		context->delaunayEdges[a][i]=nbToA[i];	

	context->allNBfrom[a]=numNB;
	context->index+=1;
	return numNB;
}

int closest(int a, int p1, int p2, Context *c) {
	assert( c != NULL);
	// return the closest point to a from p1 or p2
	if ( dist2(c->x[p1] -c->x[a], c->y[p1]-c->y[a]) < dist2(c->x[p2] -c->x[a], c->y[p2]-c->y[a]))
		return p1;
	else return p2;
}

int closestNeigbourTo(int p, Context * c){
	// start with point b = p-1 as a legal value
	assert(c != NULL);
	int b = min (p-1, c->n-1) ;
	if (b == 0) b=3;          // if p == 1
	int xp = c->x[p];
	int yp = c->y[p];
	int numBox =0;

	long long sr2 = dist2Long(xp-c->x[b], yp-c->y[b]);  // squared radius for S
	const double sr = sqrtl((double)sr2);

	int lowy = max(0, (int)(yp-sr)/c->BOX_SIDE);
	int highy= min(c->NUM_Y_BOXES-1, (int)(yp+sr)/c->BOX_SIDE);
	int lowx = max(0, (int)(xp-sr)/c->BOX_SIDE);
	int highx = min(c->NUM_X_BOXES-1, (int)(xp+sr)/c->BOX_SIDE);

	int xx,j;
	for (xx = lowx; xx <= highx; xx++) {
		for (j = c->box[xx][lowy]; j < c->box[xx][highy+1];j++) {
			if (j!= p && j != b){
				long long d2 = dist2Long(c->x[j]-xp,c->y[j]-yp);
				if ( d2 < sr2) {
					b = j;
					sr2 = d2;
				}
			}
		} 
	} 
	return b;
}

int findCoCircle(int a, int b,int coCircular, int *coCirc, Context *c){
	assert(c != NULL);
	long long dMin2 = c->MAX_X*c->MAX_X;
	long long d2;
	int *lineC1p = (int*)calloc(3,sizeof(int));
	int c1=-1,p,q,i,j;

	// add a & b for first test
	coCirc[coCircular] = a;
	coCirc[coCircular+1] = b;

	//find p closest to origin
	for ( i = 0; i < coCircular+2; i++) {
		p = coCirc[i];
		d2 = c->x[p]*c->x[p];
		if (c1 < 0 ||(d2 == dMin2 && c->y[p] < c->y[c1])|| d2 < dMin2){
			c1 = p; dMin2 =d2;
		}
	} // found candidate c1 closest to origo
	if (c1 == b) {
		// find as c the next point to the left of a
		for ( i = 0; i < coCircular; i++) {
			p = coCirc[i];
			line(c,lineC1p,c1,p);
			for ( j = 0; j < coCircular; j++) {
				q = coCirc[j];
				if (p!=q){
					if (  cAbovepB (q, lineC1p,c->x,c->y)) {
						break ;  // test next point i
					}
				} // test all points q != p
			} // end j, test all cocircle points q for above line: a-p
			if (j == coCircular) {
				// reached end of j-loop without finding a point above c1-p
				c1 = p; // p is our point because no points q above it
				break; // this i-loop is finished
			}
		} // end i
	} else if ( c1== a ) {
		// find as c the next point to the right of b

		for ( i = 0; i < coCircular; i++) {
			p = coCirc[i];

			line(c,lineC1p,c1,p);	//disse to var returnert til lineC1p

			for ( j = 0; j < coCircular; j++) {
				q = coCirc[j];
				if (p!=q){
					if ( ! cAbovepB (q, lineC1p,c->x,c->y)) {
						break ;  // test next point i
					} // end if
				}//  end test all points q != p
			} // end j,  test all cocirc points  for above line: a-p
			if (j == coCircular) {
				c1 = p; // p is our point because no points q below it
				break; // this i-loop is finished
			}
		} // end i
	} // end if c1==a

	free(lineC1p);	
	lineC1p=NULL;	

	return c1; 
} 

int findNextNeighbour(int a, int b, Context *context){
	assert(context !=NULL );
	int c = -1; 	// not legal value

	double cr2 = 0;     // Dummy start , squared dist c to S
	int mx = (context->x[a]+context->x[b])/2;
	int my = (context->y[a]+context->y[b])/2;
	// find search-circle S center (sx,sy) along midtnormal a-b
	double dx = ((context->y[b] - context->y[a])*5.0)/17.0;
	double dy = ((context->x[b] - context->x[a])*5.0)/17.0; // angle c appr. = 60 deg.	

	int *lineAB = (int*)calloc(3,sizeof(int));
	lineOverload(lineAB, context->x[a],context->y[a],context->x[b],context->y[b]);
	// bestem midtnormal a-b
	double *midNormAB = (double*)malloc(sizeof(double)* 3);	
	midNormal(a,b,midNormAB, context);	
	double *midNormAP = (double*)malloc(sizeof(double)* 3);
	double sx = mx; 
	double sy = my;

	// Central loop - loop until at least one potential c (p) is found - pick best
	do {

		sx = sx-dx;
		sy = sy+dy;
		double sr2 = dist2( (context->x[a] - sx), (context->y[a] - sy) );  // squared radius for S
		double sr;

		sr =  sqrtl(sr2);          //  radius for S

		int lowy = max(0, (int)(sy-sr)/context->BOX_SIDE);
		int highy= min(context->NUM_Y_BOXES-1, (int)(sy+sr)/context->BOX_SIDE);
		int lowx = max(0, (int)(sx-sr)/context->BOX_SIDE);
		int highx = min(context->NUM_X_BOXES-1, (int)(sx+sr)/context->BOX_SIDE);

		double minDistToAB = 10000000.0;
		double *crossing = (double*)calloc(2,sizeof(double));
		int *coCirc = (int*)malloc(sizeof(int)*10);
		int coCircular =0;	//antall punkter pÃ¥ sirklen
		int xx;
		int outer_loop=0;
		int inner_loop=0;

		for (xx = lowx; xx <= highx; xx++) {
			outer_loop++;
			int p;
			int counter=0;

			for (p = context->box[xx][lowy]; p < context->box[xx][highy+1];  p++) {
				inner_loop++;


				if (p != a && p!=b) {
					double radp2 = (context->x[p]-sx)*(context->x[p]-sx)+(context->y[p]-sy)*(context->y[p]-sy);


					if ( radp2 <= sr2 &&  cAbovepB(p, lineAB,context->x,context->y)) {

						midNormal(a,p,midNormAP,context); // Find midnormal A-C

						midNormCrossing (midNormAB,midNormAP,crossing);
						// normal, all points above ab are not colinear points with ab
						double dist = distToLine(lineAB,crossing[0],crossing[1]);
						if (abs(dist - minDistToAB) < context->EPSILON  && c != p){
							//For å feilsøke avrundings feil:
							/*d("sr=%0.16f\n",sr);
							  d("sr2=%0.16f\n",sr2);
							  d("\n",sr2);

							  d("fabsl(%0.1f-%0.1f) = %0.1f\n",dist,minDistToAB,abs(dist - minDistToAB) ); //lager utskrift til oppgaven...
							  d("abs(dist - minDistToAB) < context->EPSILON  && c != p)=%d\n",(abs(dist - minDistToAB) < context->EPSILON  && c != p));
							  d("\n");	*/
							if(coCircular == 0) {
								// must add a,b and best c found so far (for later calculations)
								coCirc[coCircular++] = c;
							}
							coCirc [coCircular++] = p;
							counter +=1;

						} else if ( dist < minDistToAB) {
							c = p;	//dette sakl ikke skje
							minDistToAB = dist;
							coCircular = 0;
						}
						// end not cocircular
					}// end usable point p
				}// end p!= a,b

			} // end new candidate point p for c in search circle
		} // end xx

		// must find right CoCircular point if needed
		if (coCircular > 0) {
			// must choose the same cocirc point as all other a,b on that circle
			c = findCoCircle(a,b,coCircular, coCirc,context);
			context->numCoCircular++;
			coCircular = 0;
		}
		dx +=dx;
		dy+= dy;

		free(crossing);
		crossing=NULL;

		free(coCirc);
		coCirc=NULL;	


	} while ( c < 0);

	free(lineAB);
	lineAB=NULL;

	free(midNormAB);
	midNormAB=NULL;

	free(midNormAP);
	midNormAP=NULL;

	return c;
}

int delaunayTriangulation (CoHullMap *coHullMap, NList *chull, Context *c) {
	assert(coHullMap != NULL);	
	assert(chull != NULL);	
	assert(c != NULL);	

	int b=0;
	int stop = 0;
	int  num = 0;
	int i;
	int a;
	// a) first find all neighbours to cohull
	for (i = 0; i <= chull->numberOfElements-1; i++){
		if ( i == 0)  stop  =  chull->dEdges[ chull->numberOfElements-1]; 
		else stop  = chull->dEdges[i-1];
		if (i == chull->numberOfElements-1) b = chull->dEdges[0]; 
		else b= chull->dEdges[i+1];
		num += findRestOfNeighbours(chull->dEdges[i], b, stop, c);
	}
	// b) then find first closest neighbour, and then

	//    rest of neighbours to all points a
	for (a = 0; a < c->n; a++) {
		if ( ! CMContains(coHullMap,a)) {
			b = closestNeigbourTo(a,c);
			//b = closest(a,c);	//optimalisering som ikke er implementert
			c->allNeighbours[0] = b;
			num += findRestOfNeighbours(a,b,b, c); // stop when finding b again
		}

	}
	return num;
}
