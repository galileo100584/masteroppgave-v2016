#include <stdio.h>

int amount_tests = 0;
int failed_tests = 0;

void test_failed(char *message) {
    printf("Test failed: %s\n", message);
    amount_tests += 1;
    failed_tests += 1;
}
void test_success() {
    amount_tests += 1;
}

void assertEqual(int value_a, int value_b, char *error_message) {
    if(value_a == value_b)
        test_success();
    else
        test_failed(error_message);
}
void assertNotEqual(int value_a, int value_b, char *error_message) {
    if(value_a != value_b)
        test_success();
    else
        test_failed(error_message);
}
void printTestResult() {
    printf("Ran %d tests. %d tests failed\n", amount_tests, failed_tests);
    if(failed_tests == 0)
        printf("\nSUCCESS\n\n");
    else
        printf("\nFAILED\n\n");
}
