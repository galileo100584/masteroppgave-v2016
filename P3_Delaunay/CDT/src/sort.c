#include <stdio.h>
#include <stdlib.h>

#include "../includes/util.h"

void  insertSort(int * a, int *y, int low, int high) {
	int i, t,ty;
	int k;
	for (k = low ; k < high; k++){
		t =  a[k+1];
		ty = y[k+1];
		i = k;

		while (i >= low && a[i] > t) {
			a[i+1] = a[i];
			y[i+1] = y[i];
			i--;
		}
		a[i+1] = t;
		y[i+1] = ty;
	}
} 

void radixSort ( int *a, int *b,int low, int high, int *y, int *yb, int maskLength, int shift, int a_length){
	int  acumVal = 0, j, n = a_length;
	int mask = (1<<maskLength) - 1;

	int * count = (int*)calloc((mask+1),sizeof(int));	//count for liten??
	// new variables because of no backcopy of b to a
	int ind=0;
	int alow=0;	

	if (a_length > (high-low+1)) {
		// normal first  digit, soritng orig 'a' to shorter 'b'
		alow =0;
	}else {// second digit, soritng shorter 'b' to orig 'a'
		alow =low;
	}
	// a) count=the frequency of each radix value in a
	int i=0; 
	for (i = low; i < high; i++)
		count[(a[i-alow]>> shift) & mask]+=1;

	// b) Add up in 'count' - accumulated values
	for (i = 0; i <= mask; i++) {
		j = count[i];
		count[i] = acumVal;
		acumVal += j;
	}
	
	// c) move numbers in sorted order a to b
	for (i = low; i < high; i++) {
		ind =  count[(a[i-alow]>>shift) & mask]++;
		b[ind+alow]  = a[i-alow];
		yb[ind+alow] = y[i-alow];
	}// end move x (in a[]) and y
	/*free(count);
	count=NULL;*/

}

void radix2(int * a, int *y, int low, int high, int a_length){
	if (low < high) {
		if ( high - low < 100 ) {
			insertSort(a, y, low, high-1) ;
		} else {

			// 2 digit radixSort on a[low..high>, let y do same moves
			int max = a[low], numBit = 2;
			int i;
			for (i = low+1 ; i < high ; i++)
				if (a[i] > max) max = a[i];

			while (max >= (1<<numBit) )numBit++;

			int bit1 = numBit/2,
			    bit2 = numBit-bit1;

			int *b = (int*)calloc((high-low+1),sizeof(int));
			int *yb = (int*)calloc((high-low+1),sizeof(int));

			radixSort( a,b,low,high,y,yb,bit1, 0,a_length);
			radixSort( b,a,low,high,yb,y,bit2, bit1,(high-low+1) );	//Dette kan ikke være samme som a_length...	
			
			/*free(b);
			b=NULL;
			
			free(yb);
			yb=NULL;*/
		} 
	}
}
