#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <assert.h>

#include "../includes/nlist.h"
#include "../includes/context.h"

int START_NUM_NEIGHBOURS = 7;	

void NListNew(NList *nlist) {
	assert(nlist != NULL);
	nlist->numberOfElements = 0;

	nlist->dEdges_length = START_NUM_NEIGHBOURS;
	nlist->dEdges = (int*)malloc(sizeof(int) * START_NUM_NEIGHBOURS);

	assert(nlist->dEdges != NULL);
}

void NListDump(NList *nlist, Context *context, char *s) {
	assert(nlist);
	assert(context);
	printf("Dump av liste: %s\n", s);

	int i;
	for(i = 0; i < nlist->numberOfElements; i++) {
		int t = nlist->dEdges[i];
		printf("%d Point %d: (%d, %d)\n", i, t, context->x[t],
				context->y[t]);
	}
}

void NListPutAfter(NList *nlist, int delaunayEdge, int suc) {
	// If list is full, double the size of the list
	assert(nlist);
	if(nlist->numberOfElements == nlist->dEdges_length) {
		int new_length = nlist->dEdges_length * 2;
		int *b = (int*)malloc(sizeof(int) * new_length);

		int i;
		for(i = 0; i < nlist->dEdges_length; i++)
			b[i] = nlist->dEdges[i];

		free(nlist->dEdges);
		nlist->dEdges = NULL;
		nlist->dEdges = b;
		nlist->dEdges_length = new_length;
	}

	int i = nlist->numberOfElements - 1;
	while(i >= 0 && nlist->dEdges[i] != suc)
		nlist->dEdges[i+1] = nlist->dEdges[i--];

	nlist->dEdges[i+1] = delaunayEdge;

	nlist->numberOfElements += 1;
}

void NListPut(NList *nlist, int delaunayEdge) {
	assert(nlist);
	if(nlist->numberOfElements == nlist->dEdges_length) {
		int new_length = nlist->dEdges_length * 1.41;

		int *b = (int*)malloc(sizeof(int) * new_length);

		int i;
		for(i = 0; i < nlist->dEdges_length; i++)
			b[i] = nlist->dEdges[i];

		free(nlist->dEdges);
		nlist->dEdges = NULL;
		nlist->dEdges = b;
		nlist->dEdges_length = new_length;
	}

	nlist->dEdges[nlist->numberOfElements++] = delaunayEdge;
}
void destroy_nlist(NList *nlist){
	free(nlist->dEdges);
	nlist->dEdges=NULL;
}
