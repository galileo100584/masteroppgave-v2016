#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <assert.h>
#include <mpi.h>

#include "../includes/util_parallel.h"	

#define MAX_BUFF 50
void gather_all(Context *c, int my_rank, int num_procs){
	int i,j,p;
	int *row;
	int count;
	MPI_Status status;

	//Dersom flere enn 2 prosesser, må meldingen samles inn fra en prosess av gangen, og med src id
	//	for(p=1;p<num_procs;p++){
	for(i=0;i<c->n;i++){


		if(my_rank==0){
			MPI_Recv(&count,1,MPI_INT,1,888,MPI_COMM_WORLD,&status);

			if(count != 0){
				row = (int*)calloc(count,sizeof(int));		//maks er egentlig n-1
				c->delaunayEdges[i]=(int*)calloc(count,sizeof(int));
				MPI_Recv(row,count,MPI_INT,1,DT_EDGES,MPI_COMM_WORLD,&status);
				c->allNBfrom[i]=count;
				c->delaunayEdges[i]=row;	//Man må mappe dette selv, ellers kan det fort bli galt!
				//Vanligvis må man bruke en en løkke pga. ugjevnhet mellom array1 & array2
				//Men her var det ikke behov siden vi bare tar imot når size > 0
			}
		}
		else{	
			//denna må være distribuert også?
			count = c->allNBfrom[i];	//må sende '0' for å få riktig antall pakker!
			//if(count=!0)
			MPI_Send(&count,1,MPI_INT,0,888,MPI_COMM_WORLD);

			if(count != 0)
				MPI_Send(c->delaunayEdges[i],count,MPI_INT,0,DT_EDGES,MPI_COMM_WORLD);
		}
	}
	//	}
}

void read_file(int *x, int *y, int length, Context *c,char *pointFile){
	FILE *myFile;
	myFile = fopen(pointFile, "r");

	int n=length;
	int BOX_SIDE,MAX_X,MAX_Y,SCALING;	
	//read file into array
	fscanf(myFile, "%d", &n);		
	fgetc(myFile);	
	fscanf(myFile, "%d", &BOX_SIDE);
	fgetc(myFile);	
	fscanf(myFile, "%d", &MAX_X);
	fgetc(myFile);	
	fscanf(myFile, "%d", &MAX_Y);
	fgetc(myFile);	
	fscanf(myFile, "%d", &SCALING);
	c->SCALING=SCALING;

	int i,j=0;

	for (i = 0; i < n; i++)
	{
		fscanf(myFile, "%d", &x[i]);
		fgetc(myFile);	//tar bort mellomrom...
		fscanf(myFile, "%d", &y[j]);
		j++;	
	}
	fclose(myFile);
}

int write_output_root(Context *c, int num_procs){
	FILE *fp;
	fp = fopen("dt_result_par_ny.txt","w+");

	fprintf(fp,"%d %d %d %d\n",c->n,c->BOX_SIDE,c->MAX_X,c->MAX_Y);

	int i,j;
	for(i=0;i<c->n;i++){	//dersom root skal skrive ut, hopper over egen del...
		fprintf(fp,"%d %d %d ",i,c->x[i],c->y[i]);
		//d("i=%d,allNB=%d\n",i,c->allNBfrom[i]);
		for(j=0;j<c->allNBfrom[i];j++){
			fprintf(fp,"%d ",c->delaunayEdges[i][j]);
		}
		fprintf(fp,"\n");
	}	
	fclose(fp);
	return 0;
}

int write_output_seq(Context *c, int num_procs,int my_rank){
	FILE *fp;
	char pointFile[32];
	snprintf(pointFile, sizeof(pointFile), "Data_proc%d.r", my_rank);
	fp = fopen(pointFile,"w+");

	if(my_rank==0) fprintf(fp,"%d %d %d %d\n",c->n,c->BOX_SIDE,c->MAX_X,c->MAX_Y);
	//if(my_rank==0)	printf("%d %d %d %d\n",c->n,c->BOX_SIDE,c->MAX_X,c->MAX_Y);

	int i,j;
	for(i=0;i<c->n;i++){	//dersom root skal skrive ut, hopper over egen del...
		if(c->allNBfrom[i]!=0){
			fprintf(fp,"%d %d %d ",i,c->x[i],c->y[i]);
			//d("i=%d,allNB=%d\n",i,c->allNBfrom[i]);
			for(j=0;j<c->allNBfrom[i];j++){
				fprintf(fp,"%d ",c->delaunayEdges[i][j]);
			}
			fprintf(fp,"\n");
		}
	}	
	fclose(fp);
	return 0;
}
int current_size(int my_rank,int *rows_recv){
	int ret=0,i;
	for(i=0;i<my_rank;i++){
		ret+=rows_recv[i]*20*sizeof(int);
	}
	return ret;
}

//skirver riktig uten junk, koster mer å bruke
int write_parallel2(Context *context, int num_procs, int my_rank, MPI_Status status){
	int *rows_recv = (int*)calloc(num_procs,sizeof(int)); 
	MPI_Allgather(&context->rows, 1, MPI_INT, rows_recv, 1, MPI_INT,MPI_COMM_WORLD);

	//rows må være et array og distribueres, da er det mulig å få tilgang til hvor mange bytes hver proc skriver
	//for så å kompanser med riktig verdi på offset
	//størrelsen på bufferne kan være litt forskjelig avhengig av antall rader hver proc har...
	int size = (context->rows*20);
	int  *buffer = (int*)malloc(sizeof(int)*size);

	//Her må hver proc ha sin egen lengde på DT og evt. antall rader
	d("proc = %d, rows = %d\n",my_rank,rows_recv[my_rank]);
	MPI_Info info;
	MPI_File_delete("datafile", info);

	MPI_File file;
	MPI_Offset offset;
	MPI_File_open(MPI_COMM_WORLD,"datafile",
			MPI_MODE_CREATE|MPI_MODE_WRONLY,
			MPI_INFO_NULL, &file);
	int i,j,k;
	int count=0;
	int stop;
	int pad = -1;
	//Dataene ligger hulter-til-bulter i dt[][], derfor må alle N traverseres, men det er mange som er tomme så er vel ok. 	
	for(i=0;i<context->n;i++){	//dersom root skal skrive ut, hopper over egen del...
		if(context->allNBfrom[i]>0){	

			buffer[count]=i;
			count+=1;
			buffer[count]=context->x[i];
			count+=1;
			buffer[count]=context->y[i];
			count+=1;

			for(j=0;j<context->allNBfrom[i];j++){
				buffer[count]=context->delaunayEdges[i][j];				
				count+=1;
			}
			stop=context->allNBfrom[i]+3;	//de 3 første...

			//fill up the rest with '-1' padding:
			for(k=stop;k<20;k++){
				buffer[count]=pad;
				count+=1;
			}
		}
	}
	//	if(my_rank!=0){
	//her trengs count fra forige prose i rekka
	if(rows_recv[my_rank]!=0){
		offset = current_size(my_rank,rows_recv);
		d("offset=%d, proc =%d\n",offset,my_rank);
	}
	//offset må beregnes med forige proc sin count, da blir det riktig
	MPI_File_set_view(file,offset,MPI_INT,MPI_INT,"native", MPI_INFO_NULL);
	MPI_File_write(file,buffer,size,MPI_INT,&status); //Til senere...

	MPI_Offset file_size;
	MPI_File_get_size(file, &file_size); 
	d("size =%d, proc=%d\n",current_size(my_rank,rows_recv),my_rank);
	MPI_File_close(&file);		
	free(buffer);
	return 0;
}
int my_payload(int **dt, int *count, int n,int my_rank){
	int i,j;
	int payload=0;
	for(i=0;i<n;i++){
		if(count[i]!=0){
			payload+=3;
			for(j=0;j<count[i];j++){
				payload+=1;
			}
		}
	}
	/*if(my_rank==0)	
	  payload+=4;*/
	return payload;
}
//Denne er litt raskere enn write_output_seq
int write_output_parseq(Context *c, int num_procs,int my_rank){
	char pointFile2[32];
	snprintf(pointFile2, sizeof(pointFile2), "Data_proc%d.r2", my_rank);
	MPI_Status status;
	MPI_File fh;
	MPI_File_open(MPI_COMM_SELF, pointFile2, MPI_MODE_CREATE | MPI_MODE_WRONLY, MPI_INFO_NULL, &fh);

	int number_of_integers, number_of_bytes;
	number_of_integers=my_payload(c->delaunayEdges,c->allNBfrom,c->n,my_rank);
	number_of_bytes = sizeof(int) * number_of_integers;
	int *junk;
	int k=0;
	junk = (int*) malloc(number_of_bytes);

	int i,j;
	for(i=0;i<c->n;i++){	//dersom root skal skrive ut, hopper over egen del...
		if(c->allNBfrom[i]!=0){
			junk[k]=i;
			k+=1;
			junk[k]=c->x[i];
			k+=1;
			junk[k]=c->y[i];
			k+=1;
			for(j=0;j<c->allNBfrom[i];j++){
				junk[k]=c->delaunayEdges[i][j];
				k+=1;
			}
		}
	}
	MPI_File_write(fh, junk, number_of_integers, MPI_INT, &status);
	MPI_File_close(&fh);

	return 0;
}

//denne skirver riktig med med litt junk...
int write_parallel(Context *context, int num_procs, int my_rank, MPI_Status status){
	int written_chars = 0;
	int written_chars_accumulator = 0;
	int n = context->n; 


	char *charbuffer = (char*)calloc(n,MAX_BUFF);
	if (charbuffer == NULL) {
		exit(1);
	}
	char *linje = charbuffer;
	MPI_Info info;
	MPI_File_delete("test_write.txt", info);

	MPI_File file;
	MPI_Offset offset;
	MPI_File_open(MPI_COMM_WORLD,"test_write.txt",
			MPI_MODE_CREATE|MPI_MODE_WRONLY,
			MPI_INFO_NULL, &file);


	int i,j;
	int stop=0;
	int pad = 0;
	//Dataene ligger hulter-til-bulter i dt[][], derfor må alle N traverseres 	
	for(i=0;i<n;i++){	//dersom root skal skrive ut, hopper over egen del...
		if(context->allNBfrom[i]>0){	
			written_chars = snprintf((char *)charbuffer+written_chars_accumulator, (n*MAX_BUFF - written_chars_accumulator), "%d %d %d ", i, context->x[i], context->y[i]);     
			if (written_chars < 0){ exit(1); }
			written_chars_accumulator += written_chars;

			for(j=0;j<context->allNBfrom[i];j++){

				written_chars = snprintf((char *)charbuffer+written_chars_accumulator, (n*MAX_BUFF - written_chars_accumulator), "%d ", context->delaunayEdges[i][j]);    
				if (written_chars < 0){ exit(1); }
				written_chars_accumulator += written_chars;

			}
			//fill up the rest with '-1' padding later:
			int lengde = strlen(linje);

			written_chars = snprintf((char *)charbuffer+written_chars_accumulator, (n*MAX_BUFF - written_chars_accumulator), "\n");     
			if (written_chars < 0){ exit(1); }
			written_chars_accumulator += written_chars;
		}
	}
	int len = strlen(charbuffer);

	offset = n*MAX_BUFF*my_rank;
	MPI_File_seek(file,offset,MPI_SEEK_SET);
	MPI_File_write(file,charbuffer,n*MAX_BUFF,MPI_CHAR,&status); 
	MPI_File_close(&file);		
	return 0;
}

void print_array(Context *c,int my_rank) {
	assert(c != NULL);
	printf("DelaunayEdge from proc %d\n");
	int i;
	int j;
	for (i=0;i < c->n; i++){
		printf("%d %d %d ",i,c->x[i],c->y[i]);
		for (j =0; j <c->allNBfrom[i]; j++){
			if(j >0) 
				printf(" ");
			printf("%d",c->delaunayEdges[i][j]);
		}
		printf("\n");
	}
}
void dumpPoints(char *s,Context *c,int my_rank) {
	assert(c != NULL);
	printf("Dump av DataPoints: %s fra proc %d\n",s,my_rank);
	int i;
	int j;
	for (i=0;i < c->n; i++){
		printf(" Point i: %d (%d,%d) med DK til:",i,c->x[i],c->y[i]);
		for (j =0; j <c->allNBfrom[i]; j++){
			if(j >0) 
				printf(",");
			printf("%d",c->delaunayEdges[i][j]);
		}
		printf("\n");
	}
}

// Sjekk konistens av DK-kanter (Hvis a-b, så b-a
// ikke sjekk på kryssende kanter
int sjekkDK (Context *c) {
	int output=1;	//utskrift av evt. feil kanter...	
	assert(c != NULL);
	int numErr =0;
	int numPrinted =0; 
	int maxPrinted =10;
	int p=0;
	int pn=0;
	for (p = 0; p < c->n; p++) {
		//d("Curretn position %d\n",p);
		// for each delaunay nerghbour pn to p, check that p is Del-neighb to pn
		int i;
		for(i=0;i<c->n;i++){
			int ok = 0;
			int j;
			//			d("c->allNBfrom[%d]=%d\n",i,c->allNBfrom[i]);
			for(j=0;j<c->allNBfrom[i];j++){
				if (c->delaunayEdges[i][j] == p){ 
					ok = 1; 
					break;
				}	
			}
			break;
			if ( ok != 1) {
				numErr ++;
				if (output && numPrinted < maxPrinted ){
					numPrinted++;
					int k;
					for (k =0; k < c->delaunayEdges_i_length; k++){
						if(k >0) printf(",\n");
						printf("%d\n",c->delaunayEdges[p][k]);
					}
					printf("\n  D-neighb for %d: ",pn);
					for (k =0; k < c->delaunayEdges_i_length; k++){
						if(k >0) printf(",");
						printf("%d",c->delaunayEdges[pn][k]);
					}
					printf("\n");
					if ( numPrinted ==  maxPrinted)
						printf ("...............etc.................");
				} // erroroutput
			} // error
		} // end all D neighbours to p		
	} // end p - all points
	printf("Number of not matching Delaunay neghbours: %d\n",numErr);
	return numErr;
}

int max(int x, int y)
{
	return x ^ ((x ^ y) & -(x < y)); 
}

int min(int x, int y)
{
	return y ^ ((x ^ y) & -(x < y));
}

double dist2 (double x, double y){ 
	//assert( ( (x*x+y*y) > sqrt(DBL_MAX) ) != 1 ); //dette må være sant for å komme videre
	return x*x+y*y;
}

long long dist2Long (long x, long y){ 
	return x*x+y*y;
}

double distToLine(int *line, double xx, double yy){
	assert(line != NULL);   //dette må være sant for å komme videre
	return line[0] *xx +line[1]*yy + line[2];
} 


void line2 (double  *line, double ax, double ay, double bx, double by) {
	// returnes the line equation cx+dy+e =0: line[0]x+ line[1]y+ line[2]=0,
	// line from  a to b
	line[0] = ay - by;
	line[1] = bx - ax;
	line[2] = by*ax-ay*bx;
}

void midNormCrossing(double *midAB, double *midAC, double *crossing){
	// returns the crossing, x=crossing[0], y = crossing [1] of two lines midAB and midAC)
	double div = midAC[0]*midAB[1]-midAB[0]*midAC[1];
	// if (Math.abs(div) < 0.00001) return false;  // Paralell lines will not accur
	assert(crossing);
	crossing[0] = (midAC[1]*midAB[2]-midAC[2]*midAB[1])/div; // x-value
	crossing[1] = (midAC[2]*midAB[0]-midAC[0]*midAB[2])/div; // y -value
}

void midNormal (int a, int b, double *ret, Context *c) {
	// returns the midnormal from line A - B on the form ret[0] + ret[1]y+ ret[1]
	// (mx,my) midpoint an a-b,
	assert(c);
	int mx = (c->x[a]+c->x[b])/2;  // OK, x and y are even numbers
	int my = (c->y[a]+c->y[b])/2;
	double p3x; 
	double p3y = c->y[a];
	if ( c->x[b] == c->x[a]){ p3x = 2*mx;p3y=my;}  // nmidnorm is parallel with x-axis
	else if (c->y[b] == c->y[a]) {p3x =mx; p3y=2*my;} // midnorm parallel y-axis
	else p3x =(c->x[b]*c->x[b] - c->x[a]*c->x[a] + pow((double)(c->y[b] - c->y[a]),2.0)) / (2.0*(c->x[b]-c->x[a]));
	line2 (ret,mx,my,p3x,p3y);
	assert(ret != NULL);
}

int Random(int min, int max){		
	return rand() % (max-min+1)+min;
}

void line(Context *context, int *line, int a, int b) {
	assert(context!=NULL);
	line[0] = context->y[a] - context->y[b];
	line[1] = context->x[b] - context->x[a]; 
	line[2] = context->y[b] * context->x[a] -
		context->y[a] * context->x[b];
}
void lineOverload(int *line, int ax,int ay, int bx, int by) {
	// returnes the line equation cx+dy+e=0 : line[0]x+ line[1]y+ line[2]=0,
	// line from  a to b
	assert(line != NULL);
	line[0] = ay - by;
	line[1] = bx - ax;
	line[2] = by*ax-ay*bx;

}

int cAbovepB (int c, int *linepB, int *x, int *y) {
	//Precondition 
	assert(x != NULL);
	assert(y != NULL);
	assert(linepB != NULL);
	return (linepB[0]*x[c] + linepB[1]*y[c] + linepB[2]) > 0;
}
