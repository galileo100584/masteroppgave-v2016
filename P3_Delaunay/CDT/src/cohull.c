#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include "../includes/context.h"
#include "../includes/nlist.h"
#include "../includes/util.h"
#include "../includes/cohull.h"

void CMNew(CoHullMap *map, int max_length) {
	assert(map != NULL);
	map->numberOfElements = 0;
	map->max_length = max_length;
	map->data = (int*)calloc(map->max_length,sizeof(int));
}

void CMPut(CoHullMap *map, int item) { 
	assert(map != NULL);
	map->numberOfElements++;
	  map->data[item]=1;
	 
}

int CMContains(CoHullMap *map, int item) {
	assert(map !=NULL);
return map->data[item];
}

int insideRect(int p1,int p2, int i, int *x, int *y) {
	int xOK = 0;
	int  yOK = 0;
	if (x[p1] < x[p2]) xOK = x[p1] <= x[i] && x[i] <= x[p2];
	else xOK = x[p1] >= x[i] && x[i] >= x[p2];

	if (y[p1] < y[p2]) yOK = y[p1] <= y[i] && y[i] <= y[p2];
	else yOK = y[p1] >= y[i] && y[i] >= y[p2];

	return xOK & yOK & (p1 !=i) & (p2!=i);
}

void coHullRec(Context *context, NList *theCoHull, CoHullMap *coHullMap, int p1, int p2) {
	assert(context != NULL);
	assert(theCoHull != NULL);
	assert(coHullMap != NULL);
	int *line_p1p2 = (int*)malloc(sizeof(int) * 3);
	line(context, line_p1p2, p1, p2);

	int a = line_p1p2[0], b = line_p1p2[1], c = line_p1p2[2], p2save = p2;
	int minD2 = 0;
	int d2=0;
	int minP=-1;
	int numNeg = 0;

	int numx = abs(context->x[p1] - context->x[p2]) / context->BOX_SIDE + 1;

	int lowx=0, highx=0, lowy=0, highy=0;

	if(context->x[p1] <= context->x[p2]) {
		lowx = context->x[p1] / context->BOX_SIDE;
		highx = context->x[p2] / context->BOX_SIDE;
		lowy = 0;
		highy = max(context->y[p1] / context->BOX_SIDE,
				context->y[p2] / context->BOX_SIDE);
	}
	else {
		lowx = context->x[p2] / context->BOX_SIDE;
		highx = context->x[p1] / context->BOX_SIDE;
		lowy = min(context->y[p1] / context->BOX_SIDE,
				context->y[p2] / context->BOX_SIDE);
		highy = context->NUM_Y_BOXES - 1;
	}
	int xx;
	for(xx = lowx; xx <= highx; xx++) {
		int i;
		for(i = context->box[xx][lowy]; i < context->box[xx][highy+1]; i++) {
			if(insideRect(p1, p2, i,context->x,context->y)){
				d2 = a * context->x[i] + b * context->y[i] + c;
				if(d2 <= 0) numNeg++;
				if(d2 <= minD2) {
					minP = i;
					minD2 = d2;
				}
			}
		}
	}

	if(numNeg > 0) {
		NListPutAfter(theCoHull, minP, p1);
		CMPut(coHullMap, minP);
		if(numNeg > 1) {
			coHullRec(context, theCoHull, coHullMap, p1, minP);
			coHullRec(context, theCoHull, coHullMap, minP, p2);
		}
	}
}



int cohull(Context *context, NList *theCoHull, CoHullMap *coHullMap) {

	assert(context != NULL);
	assert(theCoHull != NULL);
	assert(coHullMap != NULL);
	int minx = 0;
	int maxx = 0;
	int miny = 0;
	int maxy = 0;

	int i;
	for(i = 1; i < context->n; i++) {
		if(context->x[i] < context->x[minx]) minx = i;
		if(context->x[i] > context->x[maxx]) maxx = i;
		if(context->y[i] < context->y[miny]) miny = i;
		if(context->y[i] > context->y[maxy]) maxy = i;
	}
	NListPut(theCoHull, minx);
	CMPut(coHullMap, minx);

	if(miny != minx) {
		NListPutAfter(theCoHull, miny, minx);
		CMPut(coHullMap, miny);
		coHullRec(context,theCoHull,coHullMap,minx, miny);
	}
	if(maxx != miny) {
		NListPutAfter(theCoHull, maxx, miny);
		CMPut(coHullMap, maxx);
		coHullRec(context,theCoHull,coHullMap,miny, maxx);
	}
	if(maxy != maxx && maxy != minx) {
		NListPutAfter(theCoHull, maxy, maxx);
		CMPut(coHullMap, maxy);
		coHullRec(context,theCoHull,coHullMap,maxx, maxy);
	}
	if(maxy != minx) {
		coHullRec(context,theCoHull,coHullMap,maxy, minx);
	}
	else {
		coHullRec(context,theCoHull,coHullMap,maxx, minx);
	}

	return theCoHull->numberOfElements;
}
