#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <assert.h>
#include <values.h>
#include "../includes/util.h"	

void read_file(int *x, int *y, int length, Context *c, char *pointFile){
	FILE *myFile;
	myFile = fopen(pointFile, "r");

	int n=length;
	int BOX_SIDE,MAX_X,MAX_Y,SCALING;	

	//read file into array
	fscanf(myFile, "%d", &n);		
	fgetc(myFile);	
	fscanf(myFile, "%d", &BOX_SIDE);
	fgetc(myFile);	
	fscanf(myFile, "%d", &MAX_X);
	fgetc(myFile);	
	fscanf(myFile, "%d", &MAX_Y);
	fgetc(myFile);	
	fscanf(myFile, "%d", &SCALING);
	c->SCALING=SCALING;

	int i,j=0;

	for (i = 0; i < n; i++)
	{
		fscanf(myFile, "%d", &x[i]);
		fgetc(myFile);	//tar bort mellomrom...
		fscanf(myFile, "%d", &y[j]);
		j++;	
	}
}

int write_output(Context *c){
	FILE *fp;
	fp = fopen("dt_result_ser.txt","w+");

	fprintf(fp,"%d %d %d %d\n",c->n,c->BOX_SIDE,c->MAX_X,c->MAX_Y);

	int i,j;
	for(i=0;i<c->n;i++){
		fprintf(fp,"%d %d %d ",i,c->x[i],c->y[i]);
		for(j=0;j<c->allNBfrom[i];j++){
				fprintf(fp,"%d ",c->delaunayEdges[i][j]);
		}
		fprintf(fp,"\n");
	}	
	fclose(fp);
	return 0;
}

void dumpPoints(char *s,Context *c) {
	assert(c != NULL);
	printf("Dump av DataPoints: %s\n",s);
	int i;
	int j;
	for (i=0;i < c->n; i++){
		printf(" Point i: %d (%d,%d) med DK til:",i,c->x[i],c->y[i]);
		for (j =0; j < c->allNBfrom[i]; j++){
				if(j >0) 
					printf(",");
				printf("%d",c->delaunayEdges[i][j]);
			}	
		printf("\n");
	}
}

// Sjekk konistens av DK-kanter (Hvis a-b, så b-a
// ikke sjekk på kryssende kanter
//noe usikkert om denne er riktig...
int sjekkDK (Context *c) {
	int output=1;	//utskrift av evt. feil kanter...	
	assert(c != NULL);
	int numErr =0;
	int numPrinted =0; 
	int maxPrinted =10;
	int p=0;
	int pn=0;
	for (p = 0; p < c->n; p++) {
		//d("i=%d\n",i);
		// for each delaunay nerghbour pn to p, check that p is Del-neighb to pn
		int i;
		for(i=0;i < c->n; i++){
			int ok = 0;
			int j;
			for(j=0;j < c->allNBfrom[i]; j++){
				//d("count=%d\n",c->allNBfrom[i]);
				if (c->delaunayEdges[i][j] == p){ 
					ok = 1; 
					break;
				}	
			}
			break;
			if ( ok != 1) {
				numErr ++;
				if (output && numPrinted < maxPrinted ){
					numPrinted++;
					int k;
					for (k =0; k < c->delaunayEdges_i_length; k++){
						if(k >0) printf(",\n");
						printf("%d\n",c->delaunayEdges[p][k]);
					}
					printf("\n  D-neighb for %d: ",pn);
					for (k =0; k < c->delaunayEdges_i_length; k++){
						if(k >0) printf(",");
						printf("%d",c->delaunayEdges[pn][k]);
					}
					printf("\n");
					if ( numPrinted ==  maxPrinted)
						printf ("...............etc.................");
				} // erroroutput
			} // error
		} // end all D neighbours to p	
	//d("Curretn position %d\n",p);
	} // end p - all points
	printf("Number of not matching Delaunay neghbours: %d\n",numErr);
	return numErr;
}

int max(int x, int y)
{
	return x ^ ((x ^ y) & -(x < y)); 
}

int min(int x, int y)
{
	return y ^ ((x ^ y) & -(x < y));
}

double dist2 (double x, double y){ 
	assert( ( (x*x+y*y) > sqrt(DBL_MAX) ) != 1 ); //dette må være sant for å komme videre
	return x*x+y*y;
}

long long dist2Long (long x, long y){ 
	return x*x+y*y;
}

double distToLine(int *line, double xx, double yy){
	assert(line != NULL);   //dette må være sant for å komme videre
	return (double)line[0] *xx +(double)line[1]*yy + (double)line[2];
} 


void line2 (double  *line, double ax, double ay, double bx, double by) {
	// returnes the line equation cx+dy+e =0: line[0]x+ line[1]y+ line[2]=0,
	// line from  a to b
	line[0] = ay - by;
	line[1] = bx - ax;
	line[2] = by*ax-ay*bx;
}

void midNormCrossing(double *midAB, double *midAC, double *crossing){
	assert(crossing != NULL);
	assert(midAB != NULL);
	assert(midAC != NULL);
// returns the crossing, x=crossing[0], y = crossing [1] of two lines midAB and midAC)
	double div = midAC[0]*midAB[1]-midAB[0]*midAC[1];
	// if (Math.abs(div) < 0.00001) return false;  // Paralell lines will not accur
	crossing[0] = (midAC[1]*midAB[2]-midAC[2]*midAB[1])/div; // x-value
	crossing[1] = (midAC[2]*midAB[0]-midAC[0]*midAB[2])/div; // y -value
}

void midNormal (int a, int b, double *ret, Context *c) {
	// returns the midnormal from line A - B on the form ret[0] + ret[1]y+ ret[1]
	// (mx,my) midpoint an a-b,
	assert(c);
	int mx = (c->x[a]+c->x[b])/2;  // OK, x and y are even numbers
	int my = (c->y[a]+c->y[b])/2;
	double p3x; 
	double p3y = c->y[a];
	if ( c->x[b] == c->x[a]){ p3x = 2*mx;p3y=my;}  // nmidnorm is parallel with x-axis
	else if (c->y[b] == c->y[a]) {p3x =mx; p3y=2*my;} // midnorm parallel y-axis
	else p3x =(c->x[b]*c->x[b] - c->x[a]*c->x[a] + pow((double)(c->y[b] - c->y[a]),2.0)) / (2.0*(c->x[b]-c->x[a]));
	line2 (ret,mx,my,p3x,p3y);
	assert(ret != NULL);
}

int Random(int min, int max){		
	return rand() % (max-min+1)+min;
}

void line(Context *context, int *line, int a, int b) {
	assert(context!=NULL);
	line[0] = context->y[a] - context->y[b];
	line[1] = context->x[b] - context->x[a]; 
	line[2] = context->y[b] * context->x[a] -
		context->y[a] * context->x[b];
}
void lineOverload(int *line, int ax,int ay, int bx, int by) {
	// returnes the line equation cx+dy+e=0 : line[0]x+ line[1]y+ line[2]=0,
	// line from  a to b
	assert(line != NULL);
	line[0] = ay - by;
	line[1] = bx - ax;
	line[2] = by*ax-ay*bx;

}

int cAbovepB (int c, int *linepB, int *x, int *y) {
	//Precondition 
	assert(x != NULL);
	assert(y != NULL);
	assert(linepB != NULL);
	return (linepB[0]*x[c] + linepB[1]*y[c] + linepB[2]) > 0;
}
