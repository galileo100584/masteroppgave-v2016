/*Skrevet av Lars-Erik Opdal H2015*/
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
//#include <mpi.h>			Dersom man øsnker å bruke MPI_Wtime fremfor time.h

#include "../includes/context.h"
#include "../includes/nlist.h"
#include "../includes/cohull.h"
#include "../includes/util.h"
#include "../includes/functions.h"
#include "../includes/randomPoints.h"
static struct timeval tm1;
static inline void start()
{
    gettimeofday(&tm1, NULL);
}

static inline void stop()
{
    struct timeval tm2;
    gettimeofday(&tm2, NULL);

    unsigned long long t = 1000 * (tm2.tv_sec - tm1.tv_sec) + (tm2.tv_usec - tm1.tv_usec) / 1000;
    printf("%llu ms\n", t);
}
#define NDEBUG
int main(int argc, char *argv[]) { 
	if(argc < 3) {
		printf("Usage: ./delaunay <n num points> <n points per box>\n");
		return 0;
	}

	char pointFile[32];
	int number_of_points = (int) strtol(argv[1], (char **)NULL, 10);
	int points_per_box = (int) strtol(argv[2], (char **)NULL, 10);

	snprintf(pointFile, sizeof(pointFile), "Data%dpkt.txt", number_of_points);
	double t0,t1,t_start,t_end;
start();
	// Step 1, initialize
	//t_start=MPI_Wtime();
	Context *context = (Context*)malloc(sizeof(Context));

	Points *p = (Points*)malloc(sizeof(Points)*number_of_points);
	newContext(context, number_of_points, points_per_box,pointFile); //initite()
	NList *theCoHull = (NList*)malloc(sizeof(NList)); 
	NListNew(theCoHull);
	CoHullMap *coHullMap = (CoHullMap*)malloc(sizeof(CoHullMap));
	CMNew(coHullMap, context->n);
	

	//t_end=MPI_Wtime();
	//printf("Steg 1 tok %.10f s\n", t_end - t_start);
	// STEP 1 COMPLETE

	// Step 2, find convex hull
	//t0=MPI_Wtime();
	//t_start=MPI_Wtime();
	int numCohull = cohull(context, theCoHull, coHullMap);
	//t_end=MPI_Wtime();
	//printf("Steg 2 tok %.10f s\n", t_end - t_start);
	// STEP 2 COMPLETE

	// Step 3) Find near neighbours and dt edges - do not need the convex hull
//	t_start=MPI_Wtime();
	context->numberOfNeigbours = delaunayTriangulation(coHullMap,theCoHull, context);
//	t_end=MPI_Wtime();
//	t1=MPI_Wtime();
	printf("Steg 3 tok %.10f s\n", t_end - t_start);
	// STEP 3 COMPLETE
	
	//step 4) Check result & write solution
	printf("Total tid var %.10f s\n", (t1-t0));
	sjekkDK(context);	
	write_output(context);
//	t1=MPI_Wtime();
	
	// STEP 4 COMPLETE
	stop();
	// cleaning up & freeing mem
	free(p);
	p=NULL;

	free(coHullMap);
	coHullMap=NULL;

	free(theCoHull);
	theCoHull=NULL;

	destroyContext(context);
	return 0;
}
