/*Skrevet av Lars-Erik Opdal H2015*/
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <mpi.h>

#include "../includes/context.h"
#include "../includes/nlist.h"
#include "../includes/cohull.h"
#include "../includes/util_parallel.h"
#include "../includes/functions_parallel.h"

#define NDEBUG
int main(int argc, char *argv[]) { 
	int number_of_points;
	int points_per_box;

	int num_procs;
	int my_rank;
	char pointFile[32];
	MPI_Init(&argc,&argv);
	MPI_Comm_rank(MPI_COMM_WORLD,&my_rank);
	MPI_Comm_size(MPI_COMM_WORLD,&num_procs);
	MPI_Status status;

	struct timespec start_time_root;
	struct timespec end_time_root;
	double t0,t1,t_start,t_end;
	t_start=MPI_Wtime();
	t0=MPI_Wtime();

	if(my_rank==0){
		if(argc < 3) {
			printf("Usage: ./delaunay <n num points> <n points per box>\n");
			return 0;
		}
		else{
			number_of_points = (int) strtol(argv[1], (char **)NULL, 10);
			points_per_box = (int) strtol(argv[2], (char **)NULL, 10);
		}
	}

	MPI_Bcast(&number_of_points,1,MPI_INT,0,MPI_COMM_WORLD);
	MPI_Bcast(&points_per_box,1,MPI_INT,0,MPI_COMM_WORLD);

	snprintf(pointFile, sizeof(pointFile), "Data%dpkt.txt", number_of_points);

	struct timespec start_time;
	struct timespec end_time;

	// Step 1, initialize
	clock_gettime(CLOCK_REALTIME, &start_time);
	Context *context = (Context*)malloc(sizeof(Context));
	newContext(context, number_of_points, points_per_box,pointFile); //initite()//pointFile
	NList *theCoHull = (NList*)malloc(sizeof(NList)); 
	NListNew(theCoHull);
	CoHullMap *coHullMap = (CoHullMap*)malloc(sizeof(CoHullMap));
	CMNew(coHullMap, context->n);	//legge på litt ekstra her, men trenger ikke dersom max_length_cohull = sqrt(n)...?

	// STEP 1 COMPLETE
	t_end=MPI_Wtime();
	if(my_rank==0){

		dumpPoints("etter randomPoints med root\n",context,my_rank);
		d("n = %d, (10e%d)\n",context->n,(int)log10((double)context->n));
		printf("Steg 1 tok %.4f ms\n", (t_end - t_start)*1000.0);

	}

	t_start=MPI_Wtime();
	// Step 2, find convex hull
	int numCohull = cohull(context, theCoHull, coHullMap);
	// STEP 2 COMPLETE
	t_end=MPI_Wtime();
	if(my_rank==0)	printf("Steg 2 tok %.4f ms\n", t_end - t_start);

	// Step 3) Find near neighbours and dt edges - do not need the convex hull
	clock_gettime(CLOCK_REALTIME, &start_time);
	t_start=MPI_Wtime();

	//Alle prosesser skal gå inn her:
	context->numberOfNeigbours = delaunayTriangulation(coHullMap,theCoHull, context, my_rank, num_procs);
	t_end=MPI_Wtime();

	if(my_rank==0){
		printf("Steg 3 tok %.4f ms\n",(t_end-t_start)*1000.0);
		//dumpPoints("etter DT med root\n",context);
	}
	// STEP 3 COMPLETE	
	//d("proc %d, with payload=%d\n",my_rank,my_payload(context->delaunayEdges,context->allNBfrom,context->n,my_rank));
	//-------------------------------------------------------------------------------------------------------	
	// STEP 4:
	t_start=MPI_Wtime();
	write_output_seq(context,num_procs,my_rank);
	//gather_all(context,my_rank,num_procs);
	//print_array(context,my_rank);  
	// STEP 4 COMPLETE
	t_end=MPI_Wtime();
	//-------------------------------------------------------------------------------------------------------
	//print_array(context,my_rank);
	if(my_rank==0) printf("Steg 4 tok %.4f ms\n",(t_end-t_start)*1000.0);


	t1=MPI_Wtime();
	if(my_rank==0){	
		//write_output_root(context,num_procs);	//hører til gather_all

		double par_time_ms= ((t1-t0)*1000.0);
		double sek_time_ms=16453.8860; 	//speedup basert på ref. verdi, burde settes lokalt

		d("Total tidsbruk: %.4f ms\n",par_time_ms);
		d("Speedup: %.4f\n",sek_time_ms/par_time_ms);

	}
	// cleaning up & freeing mem
	//NB. Det no krøll her, pga. problem med n=10 og større, når punkter er generert i minne.

	/*	free(coHullMap);
		coHullMap=NULL;

		free(theCoHull);
		theCoHull=NULL;

		destroyContext(context);*/

	MPI_Finalize();
	return 0;
}
