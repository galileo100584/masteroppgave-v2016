#include<stdio.h>
#include<stdlib.h>

#include "../includes/randomPoints.h"
#include "../includes/util.h"

int main(){
	Points *p = malloc(sizeof(Points)*10);
	printf("points=%d\n",sizeof(Points));
	int n = 10;
	int *x = malloc(n*sizeof(int));
	int *y = malloc(n*sizeof(int));
	//Make som random points:
	int i;
	fyllArrayer(n,x,y,p);
	for(i=0;i<n;i++)	printf("x[%d]=%d, y[%d]=%d\n",i,x[i],i,y[i]);
	return 0;
}
