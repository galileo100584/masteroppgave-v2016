#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <assert.h>

#include "../includes/randomPoints.h"
#include "../includes/sort.h"
#include "../includes/util.h"
#include "../includes/context.h"

void newContext(Context *context, int n, int num_per_box, char *pointFile) {
	assert(context != NULL);
	context->index=0;
	context->SCALING = 5;	
	context->MIN_NUM_FOUND = 5;
	context->DebugLimit=60;
	context->n = n;
	context->NUM_PER_BOX = num_per_box;
	context->x = (int*)calloc(context->n,sizeof(int));	//kan virke som disse må være 0 fra start.
	context->y = (int*)calloc(context->n,sizeof(int));
	context->allNBfrom = (int*)calloc(context->n,sizeof(int));

	context->MAX_X = (int) sqrt(context->n) * context->SCALING + 1;


	context->MAX_Y = context->MAX_X;
	context->EPSILON = 10.0 / context->MAX_X;

	
	//context->delaunayEdges = (int **) Calloc(context->n,sizeof(int *)); //lurer litt på å bruk calloc her.
	context->delaunayEdges = (int **) malloc(context->n*sizeof(int *));
	
	int i;
	int j;

	context->NUM_BOX = (context->n / context->NUM_PER_BOX)+1;
	while(!(((int) sqrt((double) context->NUM_BOX)) *
				((int) sqrt((double) context->NUM_BOX)) == context->NUM_BOX))
		context->NUM_BOX++;

	context->NUM_X_BOXES = (int) sqrt((double) context->NUM_BOX);
	context->NUM_Y_BOXES = context->NUM_X_BOXES;

	context->BOX_SIDE = (int) (context->MAX_X / context->NUM_X_BOXES);
	if(context->NUM_X_BOXES * context->BOX_SIDE < context->MAX_X)
		context->BOX_SIDE++;

	context->box = (int **) malloc(sizeof(int *) * (context->NUM_X_BOXES + 1));
	for(i = 0; i < context->NUM_X_BOXES + 1; i++)
		context->box[i] = (int*)malloc(sizeof(int) * (context->NUM_Y_BOXES + 1));

	context->box[0][0] = 0;
	context->allNeighbours = (int*)malloc(sizeof(int) * 100);	//max naboer 20.

	read_file(context->x,context->y,context->n,context,pointFile);	//bruker denne for å lese fra fil.
	//Bruk denne for å lage punkter i minne.
	/*Points *p = (Points*)malloc(sizeof(Points));
	initPoints(n, p);
	fyllArrayer(context->x,context->y, p);
*/

	radix2(context->x,context->y,0,context->n,context->n);

	int low = 0;
	int high = 0;
	int boxIndex = 0;
	int xVal = context->BOX_SIDE;
	for(i = 0; i <= context->NUM_X_BOXES; i++) {
		boxIndex = 0;

		low = high;
		while(high < context->n && context->x[high] < xVal) high++;

		radix2(context->y,context->x,low,high,context->n);
		int yVal = context->BOX_SIDE;

		context->box[i][boxIndex] = low;

		int yInd = low;
		int j;
		for(j = 0; j < context->NUM_Y_BOXES; j++) {
			while(yInd < high && context->y[yInd] < yVal) yInd++;

			context->box[i][++boxIndex] = yInd;
			yVal += context->BOX_SIDE;
		}

		xVal += context->BOX_SIDE;

		context->box[i][context->NUM_Y_BOXES] = yInd;	
	}		
}
void printArray(int *a,char *name, int length){
	int i;
	printf("%s=\n",name);
	for(i=0;i<length;i++)
		printf("%d ",a[i]);
	printf("\n");
}
void destroyContext(Context *context) {
	assert(context);
	free(context->x);
	context->x=NULL;
	free(context->y);
	context->y=NULL;

	free(context->allNeighbours);
	context->allNeighbours=NULL;

	int i;

	for(i = 0; i < context->n; i++){
		free(context->delaunayEdges[i]);
		context->delaunayEdges[i]=NULL;
	}
	free(context->delaunayEdges);
	context->delaunayEdges=NULL;


	for(i = 0; i < context->NUM_X_BOXES; i++){
		free(context->box[i]);
		context->box[i]=NULL;
	}
	free(context->box);
	context->box=NULL;



}

void dumpBox(Context *context, char *s) {
	printf("\nBox-dump: %s ", s);
	printf("BOX-SIDE: %d ", context->BOX_SIDE);
	printf("NUM_X_BOXES: %d ", context->NUM_X_BOXES);
	printf("MAX_X: %d\n", context->MAX_X);

	int xx;
	for(xx = 0; xx <= context->NUM_X_BOXES; xx++) {
		printf("\nbox[%d][]:", xx);

		int yy;
		for(yy = 0; yy <= context->NUM_Y_BOXES; yy++) {
			printf("%d,", context->box[xx][yy]);
		}
	}

	printf("\n");
}

