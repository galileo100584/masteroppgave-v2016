#ifndef COHULL_H
#include "context.h"
#include "nlist.h"

typedef struct {
    int numberOfElements;
    int max_length;

    int *data;
} CoHullMap;

//de her bør returner int!
void CMNew(CoHullMap *map, int max_length);

void CMPut(CoHullMap *map, int item);

int CMContains(CoHullMap *map, int item);

int insideRect(int p1,int p2, int i, int *x, int *y);

void coHullRec(Context *context, NList *theCoHull, CoHullMap *coHullMap, int p1, int p2);

int cohull(Context *context, NList *nlist, CoHullMap *coHullMap);

#define COHULL_H
#endif
