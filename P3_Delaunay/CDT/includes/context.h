#ifndef CONTEXT_H
/* Delaunay model */
#include "randomPoints.h"
typedef struct {
	int *x;
	int *y;
	int n;
	int minDK;
	int  minPDK;
	int  maxDK;
	int  maxPDK;
	int **box;
	int **delaunayEdges;
	int *allNeighbours;
	int *allNBfrom;
	int index;
	int numberOfNeigbours;
	int SCALING;
	int MIN_NUM_FOUND;
	int NUM_BOX;
	int NUM_PER_BOX;
	int MAX_X;
	int MAX_Y;
	int NUM_X_BOXES;
	int NUM_Y_BOXES;
	int BOX_SIDE;
	double EPSILON;
	int DebugLimit;
	int numCoCircular;
	int delaunayEdges_i_length;
	int delaunayEdges_j_length;
	int antIndrePunkter;         
	int totAntKanter;            
	int totAntTriangler;     
	int numK4; 
	int rows;
} Context;

void newContext(Context *context, int n, int num_per_box, char *pointFile);
void destroyContext(Context *context);

#define CONTEXT_H
#endif
