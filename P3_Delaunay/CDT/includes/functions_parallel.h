#ifndef FUNCTIONS_PAR
#include "nlist.h"

int  findRestOfNeighbours (int a, int b, int stop, Context *context);

int closest(int a, int p1, int p2, Context *c);

int closestNeigbourTo(int p, Context * c);

int findCoCircle(int a, int b,int coCircular, int *coCirc, Context *c);

int findNextNeighbour(int a, int b, Context *context);

int insideRect(int p1,int p2, int i, int *x, int *y);

int delaunayTriangulation (CoHullMap *coHullMap, NList *chull, Context *c, int, int);	

int delaunayTriangulation_alt2(CoHullMap *coHullMap, NList *chull, Context *c, int my_rank, int num_procs);

#define FUNCTIONS_PAR
#endif
