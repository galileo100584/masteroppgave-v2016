#ifndef UTIL_H_PAR
#include <mpi.h>

#include "context.h"
#include <mpi.h>

void gather_all(Context *, int, int);

void send_to_root(Context *,int,int);

int write_parallel(Context *context, int num_procs, int my_rank, MPI_Status status);

int write_parallel2(Context *context, int num_procs, int my_rank, MPI_Status status);

void read_file(int *, int *, int, Context *c, char *pointFile);

int write_output_root(Context *c,int num_procs);

int write_output_seq(Context *c,int num_procs,int my_rank);

void dumpPoints(char *s,Context *c,int my_rank);

int sjekkDK(Context *c);

int max(int,int);

int min(int,int);

double dist2 (double x, double y);

long long dist2Long (long x, long y);

double distToLine(int *line, double xx, double yy);

void line2 (double  *line, double ax, double ay, double bx, double by);

void midNormCrossing(double *midAB, double *midAC, double *crossing);

void midNormal (int a, int b, double *ret, Context *c);

void line(Context *context, int *line, int a, int b);

void lineOverload(int *line, int ax,int ay, int bx, int by);

int cAbovepB (int c, int *linepB, int *x, int *y);

#define UTIL_H_PAR
#endif

#define DT_EDGES 111
#define SEND_COUNT 222

#define DEBUG 1
//For debugging; bare skriv "d" istedefor "printf"
#ifdef DEBUG
#define d printf
#else
#define d
#endif


