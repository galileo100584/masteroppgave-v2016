#ifndef NLIST
#include "context.h"

typedef struct {
    int numberOfElements;
    int p;

    int dEdges_length; 
    int *dEdges;

    Context *context;
} NList;

void NListNew(NList *list);
void NListDump(NList *list, Context *context, char *list_name);
void NListPutAfter(NList *list, int delaunayEdge, int suc);
void NListPut(NList *nlist, int delaunayEdge);
void destroy_nlist(NList *nlist);
#define NLIST
#endif

