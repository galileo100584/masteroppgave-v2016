#ifndef RANDOM_POINTS
#define RANDOM_POINTS

typedef struct{
	int n;
	int scaleFactor;
	int maxXY;
	int xShift;
	int64_t seed;
	// scaleFactor * scaleFactor * n= antall mulige punkter i planet (her: 4*n)
	unsigned char *bitArr;
}Points;

void initPoints(int n, Points *p);
void fyllArrayer(int *x, int *y, Points *p);
#endif
