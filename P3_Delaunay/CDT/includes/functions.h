#ifndef FUNCTIONS
#include "nlist.h"

int  findRestOfNeighbours (int a, int b, int stop, Context *context);

int closest(int a, int p1, int p2, Context *c);

int closestNeigbourTo(int p, Context * c);

int findCoCircle(int a, int b,int coCircular, int *coCirc, Context *c);

int findNextNeighbour(int a, int b, Context *context);

int insideRect(int p1,int p2, int i, int *x, int *y);

int delaunayTriangulation (CoHullMap *coHullMap, NList *chull, Context *c);	
#define FUNCTIONS
#endif
