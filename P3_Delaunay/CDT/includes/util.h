#ifndef UTIL_H
#include "context.h"

void read_file(int *, int *, int, Context *c,char *pointFile);

int write_output(Context *c);

void dumpPoints(char *s,Context *c);

int sjekkDK(Context *c);

int max(int,int);

int min(int,int);

double dist2 (double x, double y);

long long dist2Long (long x, long y);

double distToLine(int *line, double xx, double yy);

void line2 (double  *line, double ax, double ay, double bx, double by);

void midNormCrossing(double *midAB, double *midAC, double *crossing);

void midNormal (int a, int b, double *ret, Context *c);

void line(Context *context, int *line, int a, int b);

void lineOverload(int *line, int ax,int ay, int bx, int by);

int cAbovepB (int c, int *linepB, int *x, int *y);

#define UTIL_H
#endif



#define DEBUG 1
//For debugging; bare skriv "d" istedefor "printf"
#ifdef DEBUG
#define d printf
#else
#define d
#endif

#define MAX_NEIGHBOUR 50	//ifølge Arne Maus

