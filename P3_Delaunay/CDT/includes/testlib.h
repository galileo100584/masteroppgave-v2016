#ifndef TEST_H
void printTestResult();

void assertEqual(int expected, int actual, char *error_message);
void assertNotEqual(int expected, int actual, char *error_message);

#define TEST_H
#endif
