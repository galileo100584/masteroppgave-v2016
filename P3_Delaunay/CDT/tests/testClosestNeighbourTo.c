#include <math.h>
#include <stdlib.h>

#include "../includes/testlib.h"
#include "../includes/util.h"
#include "../includes/context.h"
#include "../includes/functions.h"

#define Random(min,max) rand()%(max-min+1)+min

void initContext(Context *c){
	int max=1000;
	c->x=malloc(sizeof(int)*max);
	
	c->y=malloc(sizeof(int)*max);
	
	int i;	
	for(i=0;i<max;i++){
		c->x[i]=Random(1,100);
		c->y[i]=Random(1,100);
	}	
	c->box = (int**)malloc(max*sizeof(int*));
        for (i=0; i<max; i++)
        	c->box[i] = (int*)malloc(max*sizeof(int));

	c->n=max;
	c->NUM_X_BOXES=1;
	c->NUM_Y_BOXES=1;
	c->BOX_SIDE=1;
}

int main() {
	// Preparations for testing
	Context *context = malloc(sizeof(Context));			
	initContext(context);

	// Test 1: Teste at p = 1 gir 3
	int p = 1;
	int result = closestNeigbourTo(p, context);
	assertEqual(3, result, "Burde gitt 3");
	
	printTestResult();
	return 0;
}
